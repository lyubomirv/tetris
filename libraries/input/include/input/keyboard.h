//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

namespace input
{
namespace keyboard
{

/*!
 * \brief Key codes
 */
enum Key
{
	A,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L,
	M,
	N,
	O,
	P,
	Q,
	R,
	S,
	T,
	U,
	V,
	W,
	X,
	Y,
	Z,
	Num0,
	Num1,
	Num2,
	Num3,
	Num4,
	Num5,
	Num6,
	Num7,
	Num8,
	Num9,
	Escape,
	LControl,
	LShift,
	LAlt,
	LSystem,   // The left OS specific key: window (Windows and Linux), apple (MacOS X), ...
	RControl,
	RShift,
	RAlt,
	RSystem,   // The right OS specific key: window (Windows and Linux), apple (MacOS X), ...
	Menu,      // The Menu key
	LBracket,  // The [ key
	RBracket,  // The ] key
	SemiColon, // The ; key
	Comma,     // The , key
	Period,    // The . key
	Quote,     // The ' key
	Slash,     // The / key
	BackSlash, // The \ key
	Tilde,     // The ~ key
	Equal,     // The = key
	Dash,      // The - key
	Space,
	Return,
	Backspace,
	Tab,
	PageUp,
	PageDown,
	End,
	Home,
	Insert,
	Delete,
	Add,       // The + key
	Subtract,  // The - key
	Multiply,  // The * key
	Divide,    // The / key
	Left,
	Right,
	Up,
	Down,
	Numpad0,
	Numpad1,
	Numpad2,
	Numpad3,
	Numpad4,
	Numpad5,
	Numpad6,
	Numpad7,
	Numpad8,
	Numpad9,
	F1,
	F2,
	F3,
	F4,
	F5,
	F6,
	F7,
	F8,
	F9,
	F10,
	F11,
	F12,
	F13,
	F14,
	F15,
	Pause,

	KeyCount   // Keep last -- the total number of keyboard keys
};

}   // namespace keyboard
}	// namespace input
