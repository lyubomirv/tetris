//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

namespace input
{
namespace mouse
{

/*!
 * \brief Button
 */
enum Button
{
	Left,
	Right,
	Middle,     // The middle (wheel) mouse button
	Button0,    // extra mouse button 0
	Button1,    // extra mouse button 1

	ButtonCount // Keep last -- the total number of mouse buttons
};

enum Axis
{
	X,
	Y,
	Wheel,

	AxisCount
};

}   // namespace mouse
}	// namespace input
