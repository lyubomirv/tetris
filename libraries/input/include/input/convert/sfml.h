//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>

#include "input/keyboard.h"
#include "input/mouse.h"

namespace input
{
namespace sfml
{

inline input::keyboard::Key convertKeyboardKey(const sf::Keyboard::Key& key)
{
	switch(key)
	{
		case sf::Keyboard::A:
			return input::keyboard::A;
		case sf::Keyboard::B:
			return input::keyboard::B;
		case sf::Keyboard::C:
			return input::keyboard::C;
		case sf::Keyboard::D:
			return input::keyboard::D;
		case sf::Keyboard::E:
			return input::keyboard::E;
		case sf::Keyboard::F:
			return input::keyboard::F;
		case sf::Keyboard::G:
			return input::keyboard::G;
		case sf::Keyboard::H:
			return input::keyboard::H;
		case sf::Keyboard::I:
			return input::keyboard::I;
		case sf::Keyboard::J:
			return input::keyboard::J;
		case sf::Keyboard::K:
			return input::keyboard::K;
		case sf::Keyboard::L:
			return input::keyboard::L;
		case sf::Keyboard::M:
			return input::keyboard::M;
		case sf::Keyboard::N:
			return input::keyboard::N;
		case sf::Keyboard::O:
			return input::keyboard::O;
		case sf::Keyboard::P:
			return input::keyboard::P;
		case sf::Keyboard::Q:
			return input::keyboard::Q;
		case sf::Keyboard::R:
			return input::keyboard::R;
		case sf::Keyboard::S:
			return input::keyboard::S;
		case sf::Keyboard::T:
			return input::keyboard::T;
		case sf::Keyboard::U:
			return input::keyboard::U;
		case sf::Keyboard::V:
			return input::keyboard::V;
		case sf::Keyboard::W:
			return input::keyboard::W;
		case sf::Keyboard::X:
			return input::keyboard::X;
		case sf::Keyboard::Y:
			return input::keyboard::Y;
		case sf::Keyboard::Z:
			return input::keyboard::Z;
		case sf::Keyboard::Num0:
			return input::keyboard::Num0;
		case sf::Keyboard::Num1:
			return input::keyboard::Num1;
		case sf::Keyboard::Num2:
			return input::keyboard::Num2;
		case sf::Keyboard::Num3:
			return input::keyboard::Num3;
		case sf::Keyboard::Num4:
			return input::keyboard::Num4;
		case sf::Keyboard::Num5:
			return input::keyboard::Num5;
		case sf::Keyboard::Num6:
			return input::keyboard::Num6;
		case sf::Keyboard::Num7:
			return input::keyboard::Num7;
		case sf::Keyboard::Num8:
			return input::keyboard::Num8;
		case sf::Keyboard::Num9:
			return input::keyboard::Num9;
		case sf::Keyboard::Escape:
			return input::keyboard::Escape;
		case sf::Keyboard::LControl:
			return input::keyboard::LControl;
		case sf::Keyboard::LShift:
			return input::keyboard::LShift;
		case sf::Keyboard::LAlt:
			return input::keyboard::LAlt;
		case sf::Keyboard::LSystem:
			return input::keyboard::LSystem;
		case sf::Keyboard::RControl:
			return input::keyboard::RControl;
		case sf::Keyboard::RShift:
			return input::keyboard::RShift;
		case sf::Keyboard::RAlt:
			return input::keyboard::RAlt;
		case sf::Keyboard::RSystem:
			return input::keyboard::RSystem;
		case sf::Keyboard::Menu:
			return input::keyboard::Menu;
		case sf::Keyboard::LBracket:
			return input::keyboard::LBracket;
		case sf::Keyboard::RBracket:
			return input::keyboard::RBracket;
		case sf::Keyboard::SemiColon:
			return input::keyboard::SemiColon;
		case sf::Keyboard::Comma:
			return input::keyboard::Comma;
		case sf::Keyboard::Period:
			return input::keyboard::Period;
		case sf::Keyboard::Quote:
			return input::keyboard::Quote;
		case sf::Keyboard::Slash:
			return input::keyboard::Slash;
		case sf::Keyboard::BackSlash:
			return input::keyboard::BackSlash;
		case sf::Keyboard::Tilde:
			return input::keyboard::Tilde;
		case sf::Keyboard::Equal:
			return input::keyboard::Equal;
		case sf::Keyboard::Dash:
			return input::keyboard::Dash;
		case sf::Keyboard::Space:
			return input::keyboard::Space;
		case sf::Keyboard::Return:
			return input::keyboard::Return;
		case sf::Keyboard::Back:
			return input::keyboard::Backspace;
		case sf::Keyboard::Tab:
			return input::keyboard::Tab;
		case sf::Keyboard::PageUp:
			return input::keyboard::PageUp;
		case sf::Keyboard::PageDown:
			return input::keyboard::PageDown;
		case sf::Keyboard::End:
			return input::keyboard::End;
		case sf::Keyboard::Home:
			return input::keyboard::Home;
		case sf::Keyboard::Insert:
			return input::keyboard::Insert;
		case sf::Keyboard::Delete:
			return input::keyboard::Delete;
		case sf::Keyboard::Add:
			return input::keyboard::Add;
		case sf::Keyboard::Subtract:
			return input::keyboard::Subtract;
		case sf::Keyboard::Multiply:
			return input::keyboard::Multiply;
		case sf::Keyboard::Divide:
			return input::keyboard::Divide;
		case sf::Keyboard::Left:
			return input::keyboard::Left;
		case sf::Keyboard::Right:
			return input::keyboard::Right;
		case sf::Keyboard::Up:
			return input::keyboard::Up;
		case sf::Keyboard::Down:
			return input::keyboard::Down;
		case sf::Keyboard::Numpad0:
			return input::keyboard::Numpad0;
		case sf::Keyboard::Numpad1:
			return input::keyboard::Numpad1;
		case sf::Keyboard::Numpad2:
			return input::keyboard::Numpad2;
		case sf::Keyboard::Numpad3:
			return input::keyboard::Numpad3;
		case sf::Keyboard::Numpad4:
			return input::keyboard::Numpad4;
		case sf::Keyboard::Numpad5:
			return input::keyboard::Numpad5;
		case sf::Keyboard::Numpad6:
			return input::keyboard::Numpad6;
		case sf::Keyboard::Numpad7:
			return input::keyboard::Numpad7;
		case sf::Keyboard::Numpad8:
			return input::keyboard::Numpad8;
		case sf::Keyboard::Numpad9:
			return input::keyboard::Numpad9;
		case sf::Keyboard::F1:
			return input::keyboard::F1;
		case sf::Keyboard::F2:
			return input::keyboard::F2;
		case sf::Keyboard::F3:
			return input::keyboard::F3;
		case sf::Keyboard::F4:
			return input::keyboard::F4;
		case sf::Keyboard::F5:
			return input::keyboard::F5;
		case sf::Keyboard::F6:
			return input::keyboard::F6;
		case sf::Keyboard::F7:
			return input::keyboard::F7;
		case sf::Keyboard::F8:
			return input::keyboard::F8;
		case sf::Keyboard::F9:
			return input::keyboard::F9;
		case sf::Keyboard::F10:
			return input::keyboard::F10;
		case sf::Keyboard::F11:
			return input::keyboard::F11;
		case sf::Keyboard::F12:
			return input::keyboard::F12;
		case sf::Keyboard::F13:
			return input::keyboard::F13;
		case sf::Keyboard::F14:
			return input::keyboard::F14;
		case sf::Keyboard::F15:
			return input::keyboard::F15;
		case sf::Keyboard::Pause:
			return input::keyboard::Pause;

		case sf::Keyboard::KeyCount:
			return input::keyboard::KeyCount;
	}

	return input::keyboard::KeyCount;
}

inline input::mouse::Button convertMouseButton(const sf::Mouse::Button& button)
{
	switch(button)
	{
		case sf::Mouse::Left:
			return input::mouse::Left;
		case sf::Mouse::Right:
			return input::mouse::Right;
		case sf::Mouse::Middle:
			return input::mouse::Middle;
		case sf::Mouse::XButton1:
			return input::mouse::Button0;
		case sf::Mouse::XButton2:
			return input::mouse::Button1;

		case sf::Mouse::ButtonCount:
			return input::mouse::ButtonCount;
	}

	return input::mouse::ButtonCount;
}

}	// namespace sfml
}	// namespace input
