//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cxxtest/TestSuite.h>

#include "input/context.h"

class ContextTest : public CxxTest::TestSuite 
{
public:
	void testAddKeyboardKey()
	{
		input::Context context;
		context.addMapping(input::keyboard::A, 1);
		
		TS_ASSERT(true);
	}
	
	void testHasKeyboardKey()
	{
		input::Context context;
		context.addMapping(input::keyboard::A, 1);
		
		TS_ASSERT(context.hasMapping(input::keyboard::A));
	}
	
	void testNotHasKeyboardKey()
	{
		input::Context context;
		
		TS_ASSERT(!context.hasMapping(input::keyboard::A));
		
		context.addMapping(input::keyboard::A, 1);
		
		TS_ASSERT(!context.hasMapping(input::keyboard::B));
	}
	
	void testGetKeyboardKey()
	{
		input::Context context;
		context.addMapping(input::keyboard::A, 1);
		
		TS_ASSERT_EQUALS(context.getMapping(input::keyboard::A), 1);
	}
	
	void testAddMoreKeyboardKey()
	{
		input::Context context;
		context.addMapping(input::keyboard::A, 1);
		context.addMapping(input::keyboard::B, 123);
		context.addMapping(input::keyboard::C, 412);
		
		TS_ASSERT_EQUALS(context.getMapping(input::keyboard::A), 1);
		TS_ASSERT_EQUALS(context.getMapping(input::keyboard::B), 123);
		TS_ASSERT_EQUALS(context.getMapping(input::keyboard::C), 412);
	}

	void testAddMouseButton()
	{
		input::Context context;
		context.addMapping(input::mouse::Left, 1);

		TS_ASSERT(true);
	}

	void testHasMouseButton()
	{
		input::Context context;
		context.addMapping(input::mouse::Left, 1);

		TS_ASSERT(context.hasMapping(input::mouse::Left));
	}

	void testGetMouseButton()
	{
		input::Context context;
		context.addMapping(input::mouse::Left, 1);

		TS_ASSERT_EQUALS(context.getMapping(input::mouse::Left), 1);
	}

	void testAddMouseAxis()
	{
		input::Context context;
		context.addMapping(input::mouse::X, 1);

		TS_ASSERT(true);
	}

	void testHasMouseAxis()
	{
		input::Context context;
		context.addMapping(input::mouse::X, 1);

		TS_ASSERT(context.hasMapping(input::mouse::X));
	}

	void testGetMouseAxis()
	{
		input::Context context;
		context.addMapping(input::mouse::X, 1);

		TS_ASSERT_EQUALS(context.getMapping(input::mouse::X), 1);
	}
};
