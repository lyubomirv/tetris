//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "input/input_system.h"

#include <cxxtest/TestSuite.h>

class InputSystemTest : public CxxTest::TestSuite
{
public:
	InputSystemTest()
	{
		_context.addMapping(input::keyboard::A, 1);
		_context.addMapping(input::keyboard::B, 2);
		_context.addMapping(input::keyboard::C, 3);

		_additionalContext.addMapping(input::keyboard::D, 4);

		_context.addMapping(input::mouse::Left, 5);
		_context.addMapping(input::mouse::Right, 6);

		_context.addMapping(input::mouse::X, 7);
		_context.addMapping(input::mouse::Y, 8);

		_context.addMapping(input::mouse::Wheel, 9);
	}

	void testRegisterEvent()
	{
		_input.registerAction(1);

		TS_ASSERT(true);
	}

	void testAddContext()
	{
		input::InputSystem::ContextId contextId = _input.addContext(_context);

		TS_ASSERT(true);

		_input.setContextActive(contextId);

		TS_ASSERT(true);
	}

	void testSetContextInactive()
	{
		input::InputSystem::ContextId contextId = _input.addContext(_context);
		_input.setContextActive(contextId);

		TS_ASSERT(true);

		_input.setContextInactive(contextId);

		TS_ASSERT(true);
	}

	void testSetContextInactiveActiveState()
	{
		_input.registerState(1);
		input::InputSystem::ContextId contextId = _input.addContext(_context);
		_input.setContextActive(contextId);

		_input.keyboardKeyPressed(input::keyboard::A);

		TS_ASSERT_EQUALS(_input.getStatesCount(), 1);

		_input.setContextInactive(contextId);

		TS_ASSERT_EQUALS(_input.getStatesCount(), 0);
	}

	void testKeyboardKeyPressedAction()
	{
		_input.registerAction(1);
		_input.setContextActive(_input.addContext(_context));

		_input.keyboardKeyPressed(input::keyboard::A);

		TS_ASSERT_EQUALS(_input.getActionsCount(), 1);

		const input::Action& action = _input.getActions()[0];

		TS_ASSERT_EQUALS(action.id, 1);
	}

	void testKeyboardKeyPressedActionIsTriggeredOnce()
	{
		_input.registerAction(1);
		_input.setContextActive(_input.addContext(_context));

		_input.keyboardKeyPressed(input::keyboard::A);
		_input.keyboardKeyPressed(input::keyboard::A);
		_input.keyboardKeyPressed(input::keyboard::A);

		TS_ASSERT_EQUALS(_input.getActionsCount(), 1);

		const input::Action& action = _input.getActions()[0];

		TS_ASSERT_EQUALS(action.id, 1);
	}

	void testKeyboardKeyPressedRepeatedAction()
	{
		_input.registerRepeatedAction(1, 100);
		_input.setContextActive(_input.addContext(_context));

		_input.keyboardKeyPressed(input::keyboard::A);

		TS_ASSERT_EQUALS(_input.getActionsCount(), 1);

		const input::Action& action = _input.getActions()[0];

		TS_ASSERT_EQUALS(action.id, 1);
	}

	void testKeyboardKeyPressedRepeatedActionIsTriggeredOnce()
	{
		_input.registerRepeatedAction(1, 100);
		_input.setContextActive(_input.addContext(_context));

		_input.keyboardKeyPressed(input::keyboard::A);
		_input.keyboardKeyPressed(input::keyboard::A);
		_input.keyboardKeyPressed(input::keyboard::A);

		TS_ASSERT_EQUALS(_input.getActionsCount(), 1);

		const input::Action& action = _input.getActions()[0];

		TS_ASSERT_EQUALS(action.id, 1);
	}

	void testKeyboardKeyPressedRepeatedActionRepeats()
	{
		_input.registerRepeatedAction(1, 100);
		_input.setContextActive(_input.addContext(_context));

		_input.keyboardKeyPressed(input::keyboard::A);

		_input.update(0.05f);

		TS_ASSERT_EQUALS(_input.getActionsCount(), 0);

		_input.update(0.05f);

		TS_ASSERT_EQUALS(_input.getActionsCount(), 1);

		const input::Action& action = _input.getActions()[0];

		TS_ASSERT_EQUALS(action.id, 1);
	}

	void testKeyboardKeyReleasedRepeatedAction()
	{
		_input.registerRepeatedAction(1, 100);
		_input.setContextActive(_input.addContext(_context));

		_input.keyboardKeyPressed(input::keyboard::A);

		_input.update(0.101f);

		TS_ASSERT_EQUALS(_input.getActionsCount(), 1);

		_input.keyboardKeyReleased(input::keyboard::A);

		_input.update(0.101f);

		TS_ASSERT_EQUALS(_input.getActionsCount(), 0);
	}

	void testKeyboardKeyPressedState()
	{
		_input.registerState(1);
		_input.setContextActive(_input.addContext(_context));

		_input.keyboardKeyPressed(input::keyboard::A);

		TS_ASSERT_EQUALS(_input.getStatesCount(), 1);

		const input::State& state = _input.getStates()[0];

		TS_ASSERT_EQUALS(state.id, 1);
	}

	void testKeyboardKeyPressedStateIsTriggeredOnce()
	{
		_input.registerState(1);
		_input.setContextActive(_input.addContext(_context));

		_input.keyboardKeyPressed(input::keyboard::A);
		_input.keyboardKeyPressed(input::keyboard::A);
		_input.keyboardKeyPressed(input::keyboard::A);

		TS_ASSERT_EQUALS(_input.getStatesCount(), 1);

		const input::State& state = _input.getStates()[0];

		TS_ASSERT_EQUALS(state.id, 1);
	}

	void testKeyboardKeyReleasedState()
	{
		_input.registerState(1);
		_input.setContextActive(_input.addContext(_context));

		_input.keyboardKeyPressed(input::keyboard::A);
		_input.keyboardKeyReleased(input::keyboard::A);

		TS_ASSERT_EQUALS(_input.getStatesCount(), 0);
	}

	void testMouseButtonPressedAction()
	{
		_input.registerAction(5);
		_input.setContextActive(_input.addContext(_context));

		_input.mouseButtonPressed(input::mouse::Left);

		TS_ASSERT_EQUALS(_input.getActionsCount(), 1);

		const input::Action& action = _input.getActions()[0];

		TS_ASSERT_EQUALS(action.id, 5);
	}

	void testMouseButtonPressedState()
	{
		_input.registerState(6);
		_input.setContextActive(_input.addContext(_context));

		_input.mouseButtonPressed(input::mouse::Right);

		TS_ASSERT_EQUALS(_input.getStatesCount(), 1);

		const input::State& state = _input.getStates()[0];

		TS_ASSERT_EQUALS(state.id, 6);
	}

	void testMouseButtonReleasedState()
	{
		_input.registerState(6);
		_input.setContextActive(_input.addContext(_context));

		_input.mouseButtonPressed(input::mouse::Right);
		_input.mouseButtonReleased(input::mouse::Right);

		TS_ASSERT_EQUALS(_input.getStatesCount(), 0);
	}

	void testMouseMovedAction()
	{
		_input.setScreenSize(800, 600);
		_input.registerAction(7);
		_input.registerAction(8);
		_input.setContextActive(_input.addContext(_context));

		_input.mouseMoved(200, 100);

		TS_ASSERT_EQUALS(_input.getActionsCount(), 2);

		const input::Action& action0 = _input.getActions()[0];
		const input::Action& action1 = _input.getActions()[1];

		TS_ASSERT_EQUALS(action0.id, 7);
		TS_ASSERT_EQUALS(action1.id, 8);
	}

	void testMouseMovedRange()
	{
		_input.setScreenSize(800, 600);
		_input.registerRange(7);
		_input.registerRange(8);
		_input.setContextActive(_input.addContext(_context));

		_input.mouseMoved(200, 100);

		TS_ASSERT_EQUALS(_input.getRangesCount(), 2);

		const input::Range& range0 = _input.getRanges()[0];
		const input::Range& range1 = _input.getRanges()[1];

		TS_ASSERT_EQUALS(range0.id, 7);
		TS_ASSERT_EQUALS(range0.delta, 0);
		TS_ASSERT_EQUALS(range0.position, 200);
		TS_ASSERT_EQUALS(range0.min, 0);
		TS_ASSERT_EQUALS(range0.max, 800);

		TS_ASSERT_EQUALS(range1.id, 8);
		TS_ASSERT_EQUALS(range1.delta, 0);
		TS_ASSERT_EQUALS(range1.position, 100);
		TS_ASSERT_EQUALS(range1.min, 0);
		TS_ASSERT_EQUALS(range1.max, 600);
	}

	void testMouseMovedRangeTwice()
	{
		_input.setScreenSize(800, 600);
		_input.registerRange(7);
		_input.registerRange(8);
		_input.setContextActive(_input.addContext(_context));

		_input.mouseMoved(200, 100);
		_input.update(0.001f);
		_input.mouseMoved(250, 50);

		TS_ASSERT_EQUALS(_input.getRangesCount(), 2);

		const input::Range& range0 = _input.getRanges()[0];
		const input::Range& range1 = _input.getRanges()[1];

		TS_ASSERT_EQUALS(range0.id, 7);
		TS_ASSERT_EQUALS(range0.delta, 50);
		TS_ASSERT_EQUALS(range0.position, 250);
		TS_ASSERT_EQUALS(range0.min, 0);
		TS_ASSERT_EQUALS(range0.max, 800);

		TS_ASSERT_EQUALS(range1.id, 8);
		TS_ASSERT_EQUALS(range1.delta, -50);
		TS_ASSERT_EQUALS(range1.position, 50);
		TS_ASSERT_EQUALS(range1.min, 0);
		TS_ASSERT_EQUALS(range1.max, 600);
	}

	void testMouseMovedRangeThreeTimes()
	{
		_input.setScreenSize(800, 600);
		_input.registerRange(7);
		_input.registerRange(8);
		_input.setContextActive(_input.addContext(_context));

		_input.mouseMoved(200, 100);
		_input.update(0.001f);
		_input.mouseMoved(250, 50);
		_input.update(0.001f);
		_input.mouseMoved(255, 45);

		TS_ASSERT_EQUALS(_input.getRangesCount(), 2);

		const input::Range& range0 = _input.getRanges()[0];
		const input::Range& range1 = _input.getRanges()[1];

		TS_ASSERT_EQUALS(range0.id, 7);
		TS_ASSERT_EQUALS(range0.delta, 5);
		TS_ASSERT_EQUALS(range0.position, 255);
		TS_ASSERT_EQUALS(range0.min, 0);
		TS_ASSERT_EQUALS(range0.max, 800);

		TS_ASSERT_EQUALS(range1.id, 8);
		TS_ASSERT_EQUALS(range1.delta, -5);
		TS_ASSERT_EQUALS(range1.position, 45);
		TS_ASSERT_EQUALS(range1.min, 0);
		TS_ASSERT_EQUALS(range1.max, 600);
	}

	void testMouseMovedState()
	{
		_input.setScreenSize(800, 600);
		_input.registerState(7);
		_input.registerState(8);
		_input.setContextActive(_input.addContext(_context));

		_input.mouseMoved(200, 100);

		TS_ASSERT_EQUALS(_input.getStatesCount(), 2);

		const input::State& state0 = _input.getStates()[0];
		const input::State& state1 = _input.getStates()[1];

		TS_ASSERT_EQUALS(state0.id, 7);
		TS_ASSERT_EQUALS(state1.id, 8);
	}

	void testMouseMovedStateRelease()
	{
		_input.setScreenSize(800, 600);
		_input.registerState(7);
		_input.registerState(8);
		_input.setContextActive(_input.addContext(_context));

		_input.mouseMoved(200, 100);
		_input.mouseMoved(0, 0);

		TS_ASSERT_EQUALS(_input.getStatesCount(), 0);
	}

	void testMouseWheelMovedAction()
	{
		_input.registerAction(9);
		_input.setContextActive(_input.addContext(_context));

		_input.mouseWheelMoved(1);

		TS_ASSERT_EQUALS(_input.getActionsCount(), 1);

		const input::Action& action = _input.getActions()[0];

		TS_ASSERT_EQUALS(action.id, 9);
	}

	void testMouseWheelMovedRange()
	{
		_input.registerRange(9);
		_input.setContextActive(_input.addContext(_context));

		_input.mouseWheelMoved(1);

		TS_ASSERT_EQUALS(_input.getRangesCount(), 1);

		const input::Range& range = _input.getRanges()[0];

		TS_ASSERT_EQUALS(range.id, 9);
		TS_ASSERT_EQUALS(range.delta, 1);
		TS_ASSERT_EQUALS(range.position, 0);
		TS_ASSERT_EQUALS(range.min, -1);
		TS_ASSERT_EQUALS(range.max, 1);
	}

	void testTwoActiveContexts()
	{
		_input.registerAction(1);
		_input.registerAction(4);
		_input.setContextActive(_input.addContext(_context));
		_input.setContextActive(_input.addContext(_additionalContext));

		_input.keyboardKeyPressed(input::keyboard::A);
		_input.keyboardKeyPressed(input::keyboard::D);

		TS_ASSERT_EQUALS(_input.getActionsCount(), 2);

		TS_ASSERT_EQUALS(_input.getActions()[0].id, 1);
		TS_ASSERT_EQUALS(_input.getActions()[1].id, 4);
	}

	void testNoActiveContext()
	{
		_input.registerAction(1);
		_input.registerAction(4);
		_input.addContext(_context);
		_input.addContext(_additionalContext);

		_input.keyboardKeyPressed(input::keyboard::A);
		_input.keyboardKeyPressed(input::keyboard::D);

		TS_ASSERT_EQUALS(_input.getActionsCount(), 0);
	}

	virtual void setUp()
	{
		_input = input::InputSystem();
	}

private:
	input::Context _context;
	input::Context _additionalContext;
	input::InputSystem _input;
};
