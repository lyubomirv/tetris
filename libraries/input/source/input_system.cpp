//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "input/input_system.h"

#include <cassert>

#include "detail/convert_input_id.h"

namespace input
{

InputSystem::InputSystem()
	: _contextActive(MAX_CONTEXTS, false)
	, _totalTimeMilliseconds(0)
{
}

void InputSystem::update(float deltaTime)
{
	clearEvents();

	_totalTimeMilliseconds += std::size_t(deltaTime * 1000.0f);

	for(RepeatedActionsLastTime::iterator it = _repeatedActionsLastTime.begin();
		it != _repeatedActionsLastTime.end();
		++it)
	{
		const EventId& eventId = it->first;
		std::size_t& lastTime = it->second;

		const std::size_t repeatRate = _repeatedActionRates.get(eventId);

		if(_totalTimeMilliseconds >= lastTime + repeatRate)
		{
			_actions.push(createEvent<Action>(eventId));
			lastTime = _totalTimeMilliseconds;
		}
	}
}

void InputSystem::registerAction(const EventId& eventId)
{
	registerEventType(eventId, event::Action);
}

void InputSystem::registerRepeatedAction(const EventId& eventId, std::size_t repeatRateMilliseconds)
{
	registerEventType(eventId, event::Action);
	_repeatedActionRates.add(eventId, repeatRateMilliseconds);
}

void InputSystem::registerRange(const EventId& eventId)
{
	registerEventType(eventId, event::Range);
}

void InputSystem::registerState(const EventId& eventId)
{
	registerEventType(eventId, event::State);
}

InputSystem::ContextId InputSystem::addContext(const Context& context)
{
	return _contexts.push(context);
}

void InputSystem::setContextActive(ContextId contextId)
{
	if(contextId >= _contexts.size())
	{
		assert(false && "Invalid context id.");
		return;
	}

	assert(!doesContextOverlapWithActive(contextId) && "Context overlaps with an already active one");

	_contextActive[contextId] = true;
}

struct EventEventIdComparator
	: public std::unary_function<Event, bool>
{
	EventEventIdComparator(const EventId& eventId)
		: _eventId(eventId)
	{
	}

	bool operator() (const Event& event) const
	{
		return event.id == _eventId;
	}

private:
	const EventId& _eventId;
};

void InputSystem::setContextInactive(ContextId contextId)
{
	if(contextId >= _contexts.size())
	{
		assert(false && "Invalid context id.");
		return;
	}

	clearContextEvents(contextId);

	_contextActive[contextId] = false;
}

void InputSystem::setScreenSize(unsigned int width, unsigned int height)
{
	_screenWidth = width;
	_screenHeight = height;
}

void InputSystem::keyboardKeyPressed(const keyboard::Key& key)
{
	keyPressed(key);
}

void InputSystem::keyboardKeyReleased(const keyboard::Key& key)
{
	keyReleased(key);
}

void InputSystem::mouseButtonPressed(const mouse::Button& button)
{
	keyPressed(button);
}

void InputSystem::mouseButtonReleased(const mouse::Button& button)
{
	keyReleased(button);
}

void InputSystem::mouseMoved(short x, short y)
{
	axisMoved(mouse::X, x, 0, _screenWidth);
	axisMoved(mouse::Y, y, 0, _screenHeight);
}

void InputSystem::mouseWheelMoved(short delta)
{
	for(ContextId contextId = ContextId(); contextId < _contexts.size(); ++contextId)
	{
		if(!_contextActive[contextId])
		{
			continue;
		}

		const Context& context = _contexts[contextId];

		if(!context.hasMapping(mouse::Wheel))
		{
			continue;
		}

		EventId eventId = context.getMapping(mouse::Wheel);

		assert(_eventTypes.hasKey(eventId) && "Event not registered.");
		switch(_eventTypes.get(eventId))
		{
			case event::Action:
				_actions.push(createEvent<Action>(eventId));;
				break;
			case event::Range:
				{
					Range range = createEvent<Range>(eventId);
					range.delta = delta;
					range.position = 0;
					range.min = -1;
					range.max = 1;
					_ranges.push(range);
				}
				break;
			case event::State:
				assert(false && "It is not possible to have a state event mapped on the mouse wheel.");
				break;
		}

		// In case this mapping overlaps in several contexts.
		break;
	}
}

void InputSystem::clearEvents()
{
	_actions.clear();
	_ranges.clear();
}

std::size_t InputSystem::getActionsCount() const
{
	return _actions.size();
}

const Action* InputSystem::getActions() const
{
	return _actions.getArray();
}

std::size_t InputSystem::getRangesCount() const
{
	return _ranges.size();
}

const Range* InputSystem::getRanges() const
{
	return _ranges.getArray();
}

std::size_t InputSystem::getStatesCount() const
{
	return _states.size();
}

const State* InputSystem::getStates() const
{
	return _states.getArray();
}

template <typename KeyType>
void InputSystem::keyPressed(const KeyType& key)
{
	// Ensure that the input is processed only once.
	const Context::InputId inputId = detail::getInputId(key);
	ActiveInput::const_iterator findIt =
		std::find(_activeInput.begin(), _activeInput.end(), inputId);
	if(findIt == _activeInput.end())
	{
		_activeInput.push(inputId);
	}
	else
	{
		// This event was already processed.
		return;
	}

	for(ContextId contextId = ContextId(); contextId < _contexts.size(); ++contextId)
	{
		if(!_contextActive[contextId])
		{
			continue;
		}

		const Context& context = _contexts[contextId];

		if(!context.hasMapping(key))
		{
			continue;
		}

		EventId eventId = context.getMapping(key);

		assert(_eventTypes.hasKey(eventId) && "Event not registered.");
		switch(_eventTypes.get(eventId))
		{
			case event::Action:
				_actions.push(createEvent<Action>(eventId));;
				if(isRepeatedAction(eventId))
				{
					_repeatedActionsLastTime.set(eventId, _totalTimeMilliseconds);
				}
				break;
			case event::Range:
				assert(false && "It is not possible to have a range event mapped on a key.");
				break;
			case event::State:
				_states.push(createEvent<State>(eventId));
				break;
		}

		// In case this mapping overlaps in several contexts.
		break;
	}
}

template <typename KeyType>
void InputSystem::keyReleased(const KeyType& key)
{
	// Ensure that the input is processed only once.
	const Context::InputId inputId = detail::getInputId(key);
	ActiveInput::iterator findIt =
		std::find(_activeInput.begin(), _activeInput.end(), inputId);
	if(findIt != _activeInput.end())
	{
		_activeInput.eraseUnordered(findIt);
	}
	else
	{
		assert(false && "Event is not active");
		return;
	}

	for(ContextId contextId = ContextId(); contextId < _contexts.size(); ++contextId)
	{
		if(!_contextActive[contextId])
		{
			continue;
		}

		const Context& context = _contexts[contextId];

		if(!context.hasMapping(key))
		{
			continue;
		}

		EventId eventId = context.getMapping(key);

		assert(_eventTypes.hasKey(eventId) && "Event not registered.");
		switch(_eventTypes.get(eventId))
		{
			case event::Action:
				if(isRepeatedAction(eventId))
				{
					_repeatedActionsLastTime.erase(eventId);
				}
				break;
			case event::Range:
				assert(false && "It is not possible to have a range event mapped on a key.");
				break;
			case event::State:
				for(std::size_t i = 0; i < _states.size(); ++i)
				{
					if(_states[i].id == eventId)
					{
						_states.eraseUnordered(i);
					}
				}
				break;
		}

		// In case this mapping overlaps in several contexts.
		break;
	}
}

template <typename AxisType>
void InputSystem::axisMoved(const AxisType& axis, short position, short min, short max)
{
	const Context::InputId axisInputId = detail::getInputId(axis);

	short delta = 0;
	if(_axisPosition.hasKey(axisInputId))
	{
		delta = position - _axisPosition.get(axisInputId);
		if(delta == 0)
		{
			return;
		}
	}
	_axisPosition.set(axisInputId, position);

	for(ContextId contextId = ContextId(); contextId < _contexts.size(); ++contextId)
	{
		if(!_contextActive[contextId])
		{
			continue;
		}

		const Context& context = _contexts[contextId];

		if(!context.hasMapping(axis))
		{
			continue;
		}

		EventId eventId = context.getMapping(axis);

		assert(_eventTypes.hasKey(eventId) && "Event not registered.");
		switch(_eventTypes.get(eventId))
		{
			case event::Action:
				_actions.push(createEvent<Action>(eventId));;
				if(isRepeatedAction(eventId))
				{
					if(position != 0)
					{
						_repeatedActionsLastTime.set(eventId, _totalTimeMilliseconds);
					}
					else
					{
						_repeatedActionsLastTime.erase(eventId);
					}
				}
				break;
			case event::Range:
				{
					Range range = createEvent<Range>(eventId);
					range.delta = delta;
					range.position = position;
					range.min = min;
					range.max = max;
					_ranges.push(range);
				}
				break;
			case event::State:
				{
					if(position != 0)
					{
						_states.push(createEvent<State>(eventId));
					}
					else
					{
						for(std::size_t i = 0; i < _states.size(); ++i)
						{
							if(_states[i].id == eventId)
							{
								_states.eraseUnordered(i);
							}
						}
					}
				}
				break;
		}

		// In case this mapping overlaps in several contexts.
		break;
	}
}

void InputSystem::registerEventType(const EventId& eventId, event::Type type)
{
	if(_eventTypes.hasKey(eventId))
	{
		assert(false && "Event already registered.");
		return;
	}

	_eventTypes.add(eventId, type);
}

bool InputSystem::isRepeatedAction(const EventId& eventId) const
{
	return _repeatedActionRates.hasKey(eventId);
}

void InputSystem::clearContextEvents(const ContextId& contextId)
{
	if(contextId >= _contexts.size())
	{
		assert(false && "Invalid context id.");
		return;
	}

	const Context& context = _contexts[contextId];
	const Context::Mappings& contextMap = context.getMappings();
	for(Context::Mappings::const_iterator it = contextMap.begin();
		it != contextMap.end();
		++it)
	{
		const EventId& eventId = it->second;

		EventEventIdComparator comp(eventId);

		if(_repeatedActionsLastTime.hasKey(eventId))
		{
			_repeatedActionsLastTime.erase(eventId);
		}

		Actions::iterator acIt =
			std::find_if(_actions.begin(), _actions.end(), comp);
		if(acIt != _actions.end())
		{
			_actions.eraseUnordered(acIt);
			continue;
		}

		Ranges::iterator rIt =
			std::find_if(_ranges.begin(), _ranges.end(), comp);
		if(rIt != _ranges.end())
		{
			_ranges.eraseUnordered(rIt);
			continue;
		}

		States::iterator sIt =
			std::find_if(_states.begin(), _states.end(), comp);
		if(sIt != _states.end())
		{
			_states.eraseUnordered(sIt);
			continue;
		}
	}
}

bool InputSystem::doesContextOverlapWithActive(const ContextId& contextId) const
{
	if(contextId >= _contexts.size())
	{
		assert(false && "Invalid context id.");
		return false;
	}

	const Context& context = _contexts[contextId];
	const Context::Mappings& contextMap = context.getMappings();

	for(ContextId activeContextId = ContextId();
		activeContextId < _contexts.size();
		++activeContextId)
	{
		if(!_contextActive[activeContextId] || contextId == activeContextId)
		{
			continue;
		}

		const Context& activeContext = _contexts[activeContextId];

		for(Context::Mappings::const_iterator it = contextMap.begin();
			it != contextMap.end();
			++it)
		{
			const Context::InputId& inputId = it->first;

			if(activeContext.hasMapping(inputId))
			{
				return true;
			}
		}
	}

	return false;
}

}	// namespace input
