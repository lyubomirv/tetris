//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

// Operating system defines.
#if defined(__linux__)
	#define PLATFORM_LINUX
#elif defined(_WIN32) || defined(__WIN32__) || defined(__WINDOWS__)
	#define PLATFORM_WINDOWS
#elif defined(__APPLE__) && defined(__MACH__)
	#define PLATFORM_OSX
#else
	#error "Unsupported platform"
#endif

// Compiler defines.
#if defined(__GNUC__)
	#define COMPILER_GCC
#elif defined(_MSC_VER)
	#define COMPILER_MSVC
#endif

// Build mode defines.
#if defined(_DEBUG)
	#define MODE_DEBUG
#else
	#define MODE_RELEASE
#endif

// Platform-specific compiler definitions.
#if defined(PLATFORM_WINDOWS)
	#if !defined(NOMINMAX)
		#define NOMINMAX
	#endif
#endif
