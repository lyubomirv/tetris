//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "base/fixed_size_array.h"

#include <cxxtest/TestSuite.h>

class FixedSizeArrayTest : public CxxTest::TestSuite
{
public:
	void testPush()
	{
		base::FixedSizeArray<int, 10> array;
		array.push(1);

		TS_ASSERT(true);
	}

	void testSize()
	{
		base::FixedSizeArray<int, 10> array;

		TS_ASSERT_EQUALS(array.size(), 0);

		array.push(1);

		TS_ASSERT_EQUALS(array.size(), 1);
	}

	void testCapacity()
	{
		base::FixedSizeArray<int, 10> array;

		TS_ASSERT_EQUALS(array.capacity(), 10);

		array.push(1);

		TS_ASSERT_EQUALS(array.capacity(), 10);
	}

	void testNonConstOperatorBrackets()
	{
		base::FixedSizeArray<int, 10> array;
		array.push(1);

		TS_ASSERT_EQUALS(array[0], 1);
	}

	void testConstOperatorBrackets()
	{
		base::FixedSizeArray<int, 10> array;
		array.push(1);

		const base::FixedSizeArray<int, 10>& arrayRef = array;

		TS_ASSERT_EQUALS(arrayRef[0], 1);
	}

	void testEraseOrderedIndex()
	{
		base::FixedSizeArray<int, 10> array;
		array.push(1);
		array.push(2);
		array.push(3);
		array.push(4);
		array.push(5);

		array.eraseOrdered(1);

		TS_ASSERT_EQUALS(array[0], 1);
		TS_ASSERT_EQUALS(array[1], 3);
		TS_ASSERT_EQUALS(array[2], 4);
		TS_ASSERT_EQUALS(array[3], 5);
	}

	void testEraseOrderedIterator()
	{
		typedef base::FixedSizeArray<int, 10> Array;
		Array array;
		array.push(1);
		array.push(2);
		array.push(3);
		array.push(4);
		array.push(5);

		Array::iterator it = array.begin();
		++it;

		array.eraseOrdered(it);

		TS_ASSERT_EQUALS(array[0], 1);
		TS_ASSERT_EQUALS(array[1], 3);
		TS_ASSERT_EQUALS(array[2], 4);
		TS_ASSERT_EQUALS(array[3], 5);
	}

	void testEraseUnorderedIndex()
	{
		base::FixedSizeArray<int, 10> array;
		array.push(1);
		array.push(2);
		array.push(3);
		array.push(4);
		array.push(5);

		array.eraseUnordered(1);

		TS_ASSERT_EQUALS(array[0], 1);
		TS_ASSERT_EQUALS(array[1], 5);
		TS_ASSERT_EQUALS(array[2], 3);
		TS_ASSERT_EQUALS(array[3], 4);
	}

	void testEraseUnorderedIterator()
	{
		typedef base::FixedSizeArray<int, 10> Array;
		Array array;
		array.push(1);
		array.push(2);
		array.push(3);
		array.push(4);
		array.push(5);

		Array::iterator it = array.begin();
		++it;

		array.eraseUnordered(it);

		TS_ASSERT_EQUALS(array[0], 1);
		TS_ASSERT_EQUALS(array[1], 5);
		TS_ASSERT_EQUALS(array[2], 3);
		TS_ASSERT_EQUALS(array[3], 4);
	}

	void testIterate()
	{
		typedef base::FixedSizeArray<int, 10> Array;
		Array array;

		for(int i = 0; i < 10; ++i)
		{
			array.push(i);
		}

		int index = 0;
		for(Array::const_iterator it = array.begin(); it != array.end(); ++it, ++index)
		{
			TS_ASSERT_EQUALS(*it, index);
		}
	}
};
