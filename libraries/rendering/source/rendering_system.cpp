//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "rendering/rendering_system.h"

#include "memory/allocators.h"

namespace rendering
{

RenderingSystem::RenderingSystem()
	: _objects(memory::allocators::heap())
{
}

bool RenderingSystem::initialize(unsigned int width, unsigned int height)
{
	glewExperimental = true;
	if(glewInit() != GLEW_OK)
	{
		assert(false && "Glew not initialized.");
		return false;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glEnable(GL_CULL_FACE);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	_camera.setPerspective(45.0f, float(width) / height);
	_camera.setPosition(glm::vec3(4.0f, 3.0f, 3.0f));
	_camera.setLookAt(glm::vec3(-4.0f, -3.0f, -3.0f));
	_camera.setUp(glm::vec3(0.0f, 1.0f, 0.0f));

	return true;
}

void RenderingSystem::resize(unsigned int width, unsigned int height)
{
	_camera.setPerspective(45.0f, float(width) / height);
}

void RenderingSystem::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	const Objects::Objects& objects = _objects.objects(); // more objects? :D
	for(Objects::Objects::const_iterator it = objects.begin(); it != objects.end(); ++it)
	{
		const RenderingData& rd = *it;

		glUseProgram(rd.spId);
		const glm::mat4 projViewModel = _camera.projectionView() * rd.transform;
		const GLuint mvpId = glGetUniformLocation(rd.spId, "MVP");
		glUniformMatrix4fv(mvpId, 1, GL_FALSE, &projViewModel[0][0]);

		switch(rd.material.type)
		{
			case rendering::MaterialData::MT_Texture:
			{
				const GLuint texSamplerId = glGetUniformLocation(rd.spId, "textureSampler");
				glUniform1i(texSamplerId, 0);
				glActiveTexture(GL_TEXTURE0 + 0);
				glBindTexture(GL_TEXTURE_2D, rd.material.textureIds[0]);
				break;
			}
		}

		glBindVertexArray(rd.vaoId);
		glDrawElements(GL_TRIANGLES, rd.elementsCount, GL_UNSIGNED_SHORT, NULL);
		glBindVertexArray(0);
	}
}

RenderingSystem::Objects& RenderingSystem::objects()
{
	return _objects;
}

Camera& RenderingSystem::camera()
{
	return _camera;
}

BufferManager& RenderingSystem::bufferManager()
{
	return _bufferManager;
}

ShaderManager& RenderingSystem::shaderManager()
{
	return _shaderManager;
}

ShaderProgramManager& RenderingSystem::shaderProgramManager()
{
	return _shaderProgramManager;
}

TextureManager& RenderingSystem::textureManager()
{
	return _textureManager;
}

VertexArrayObjectManager& RenderingSystem::vaoManager()
{
	return _vaoManager;
}

}	// namespace rendering
