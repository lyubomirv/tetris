//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "rendering/buffer_manager.h"

#include <GL/glew.h>

#include "memory/allocators.h"

#include "rendering/opengl.h"

namespace rendering
{

BufferManager::BufferManager()
	: _bufferIds(memory::allocators::heap())
{
}

BufferManager::~BufferManager()
{
	for(BufferIds::const_iterator it = _bufferIds.begin(); it != _bufferIds.end(); ++it)
	{
		remove(*it);
	}
}

BufferId BufferManager::generate()
{
	BufferId bufferId = 0;
	glGenBuffers(1, &bufferId);
	_bufferIds.push_back(bufferId);

	return bufferId;
}

bool BufferManager::create(
	BufferId bufferId,
	BufferManager::BufferType type,
	const void* data,
	unsigned int byteCount)
{
	GLenum target = 0;
	switch(type)
	{
		case BT_ARRAY:
			target = GL_ARRAY_BUFFER;
			break;
		case BT_ELEMENT:
			target = GL_ELEMENT_ARRAY_BUFFER;
			break;
	}

	glBindBuffer(target, bufferId);
	glBufferData(target, byteCount, data, GL_STATIC_DRAW);
	glBindBuffer(target, 0);

	return true;
}

void BufferManager::remove(BufferId bufferId)
{
	glDeleteBuffers(1, &bufferId);
}

}	// namespace rendering
