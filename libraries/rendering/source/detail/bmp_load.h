//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

namespace rendering
{
namespace detail
{

bool getTextureDataBMP(
	const unsigned char* filedata,
	unsigned int length,
	unsigned char* outTextureData,
	unsigned int& outWidth,
	unsigned int& outHeight);

}	// namespace detail
}	// namespace rendering
