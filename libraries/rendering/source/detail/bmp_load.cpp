//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "bmp_load.h"

#include <cassert>
#include <cstring>

namespace rendering
{
namespace detail
{

bool getTextureDataBMP(
	const unsigned char* filedata,
	unsigned int length,
	unsigned char* outTextureData,
	unsigned int& outWidth,
	unsigned int& outHeight)
{
	if(length <= 54)
	{
		assert(false && "BMP Data too short.");
		return false;
	}

	if(filedata[0] != 'B' || filedata[1] != 'M')
	{
		assert(false && "Not a bmp file.");
		return false;
	}

	unsigned int dataPos = *(unsigned int*)(&filedata[0x0a]);
	const unsigned int width = *(unsigned int*)(&filedata[0x12]);
	const unsigned int height = *(unsigned int*)(&filedata[0x16]);
	unsigned int imageSize = *(unsigned int*)(&filedata[0x22]);

	if(imageSize == 0)
	{
		imageSize = width * height * 3;
	}

	if(dataPos == 0)
	{
		dataPos = 54;
	}

	memcpy(outTextureData, &filedata[dataPos], imageSize);
	outWidth = width;
	outHeight = height;

	return true;
}

}	// namespace detail
}	// namespace rendering
