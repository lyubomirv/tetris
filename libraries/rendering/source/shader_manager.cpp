//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "rendering/shader_manager.h"

#include <cassert>
#include <iostream>

#include <GL/glew.h>

#include "memory/allocators.h"

#include "rendering/opengl.h"

namespace rendering
{

ShaderManager::ShaderManager()
	: _shaderIds(memory::allocators::heap())
{
}

ShaderManager::~ShaderManager()
{
	for(ShaderIds::const_iterator it = _shaderIds.begin(); it != _shaderIds.end(); ++it)
	{
		remove(*it);
	}
}

ShaderId ShaderManager::generate(shader::Type type)
{
	GLenum shaderType;
	switch(type)
	{
		case shader::VERTEX:
			shaderType = GL_VERTEX_SHADER;
			break;
		case shader::FRAGMENT:
			shaderType = GL_FRAGMENT_SHADER;
			break;
		default:
			assert(false && "Unknown shader type.");
	}

	ShaderId shaderId = glCreateShader(shaderType);
	_shaderIds.push_back(shaderId);

	return shaderId;
}

bool ShaderManager::create(
	ShaderId shaderId,
	const char* shaderCode,
	unsigned int length)
{
	const GLint lengths[1] = { static_cast<GLint>(length) };
	glShaderSource(shaderId, 1, &shaderCode, lengths);
	glCompileShader(shaderId);

	// Check shader compilation.
	GLint result = GL_FALSE;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &result);

	if(result != GL_TRUE)
	{
		const GLsizei MAX_MESSAGE_LENGTH = 1024;
		char message[MAX_MESSAGE_LENGTH];
		GLsizei messageLength;
		glGetShaderInfoLog(shaderId, MAX_MESSAGE_LENGTH, &messageLength, message);

		/// \todo Logging.
		std::cerr
			<< "Shader compilation failed for shader: " << shaderId
			<< ". Log message: " << message
			<< std::endl;

		return false;
	}

	return true;
}

void ShaderManager::remove(ShaderId shaderId)
{
	glDeleteShader(shaderId);
}

}	// namespace rendering
