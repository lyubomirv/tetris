//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "base/platform_defines.h"

/**
 * \file opengl.h
 * \brief This file just includes the OpenGL (GL and GLU) headers,
 * which have different paths on each system.
 */

#if defined(PLATFORM_LINUX)

	#include <GL/gl.h>
	#include <GL/glu.h>

#elif defined(PLATFORM_WINDOWS)

	// The Visual C++ version of gl.h uses WINGDIAPI and APIENTRY but doesn't define them
	#if definded(COMPILER_MSVC)
		#include <windows.h>
	#endif

	#include <GL/gl.h>
	#include <GL/glu.h>

#elif defined(PLATFORM_OSX)

	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>

#else
	#error "Unsupported opengl platform."
#endif
