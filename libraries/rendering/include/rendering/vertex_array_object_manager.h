//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "mstd/vector.h"

#include "rendering/types.h"

namespace rendering
{

class VertexArrayObjectManager
{
public:
	VertexArrayObjectManager();
	~VertexArrayObjectManager();

public:
	enum BufferElementType
	{
		BFE_FLOAT
	};

public:
	VAOId generate();
	void addArrayBuffer(
		VAOId vaoId,
		BufferId bufferId,
		unsigned int index,
		unsigned int sizePerElement,
		BufferElementType bfeType);
	void addElementBuffer(
		VAOId vaoId,
		BufferId bufferId);
	void remove(VAOId vaoId);

private:
	typedef mstd::Vector<VAOId> VAOIds;
	VAOIds _vaoIds;
};

}	// namespace rendering
