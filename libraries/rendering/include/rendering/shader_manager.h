//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "mstd/vector.h"

#include "rendering/types.h"

namespace rendering
{

namespace shader
{
enum Type
{
	VERTEX,
	FRAGMENT
};
}

class ShaderManager
{
public:
	ShaderManager();
	~ShaderManager();

public:
	ShaderId generate(shader::Type type);

	/**
	 * \brief Creates a shader object, associated with the \a shaderId.
	 *
	 * The \a shaderId must be one obtained from a previous call to \a generate().
	 */
	bool create(ShaderId shaderId, const char* shaderCode, unsigned int length);

	void remove(ShaderId shaderid);

private:
	typedef mstd::Vector<ShaderId> ShaderIds;
	ShaderIds _shaderIds;
};

}	// namespace rendering
