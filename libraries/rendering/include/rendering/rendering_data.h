//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <glm/glm.hpp>

#include "base/fixed_size_array.h"
#include "rendering/types.h"

namespace rendering
{

struct MaterialData
{
	enum MaterialType
	{
		MT_Texture
	};

	MaterialType type;
	enum { MAX_TEXTURE_IDS = 5 };
	base::FixedSizeArray<TextureId, MAX_TEXTURE_IDS> textureIds;
};

struct RenderingData
{
	ObjectId id;

	VAOId vaoId;
	unsigned short elementsCount;
	ShaderProgramId spId;
	MaterialData material;
	glm::mat4 transform;
};

}	// namespace rendering
