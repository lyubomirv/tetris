//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "memory/i_allocator.h"

namespace memory
{
namespace allocators
{

/**
 * \brief Creates the system memory allocators.
 * \note Must be called exactly once at the start of an application if the
 * \a heap() and \a page() functions are to be used in it.
 */
void create();
/**
 * \brief Destroys the system memory allocators, created by \a create().
 * \note Must be called exactly once before ending an application if \a create()
 * was called in the beginning of it.
 */
void destroy();

/**
 * \brief Gives the default system heap allocator.
 * \note \a create() must be called prior to using this function.
 */
IAllocator& heap();
/**
 * \brief Gives the default system page allocator.
 * \note \a create() must be called prior to using this function.
 */
IAllocator& page();

}	// namespace allocators
}	// namespace memory
