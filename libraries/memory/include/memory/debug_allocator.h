//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "memory/i_allocator.h"

namespace memory
{

/*!
 * \class DebugAllocator
 * \brief Tracks all allocations and deallocations.
 *
 * Uses an internal locked map to track all allocations.
 * Guards against:
 *    - wrong allocation (at an already allocated address);
 *    - deallocation of an address, not allocated by the layer;
 *    - double deallocation;
 *    - missing call to deallocate of all allocations.
 * \tparam SuperLayer layer, used for allocations and deallocations
 */
class DebugAllocator: public IAllocator
{
public:
	DebugAllocator(IAllocator& baseAllocator);
	virtual ~DebugAllocator();

	virtual void* allocate(size_t size, size_t alignment);
	virtual void deallocate(void* ptr);

	virtual size_t size(void* ptr);
	virtual size_t totalSize() const;

public:
	/*!
	 * \brief Allocates memory
	 *
	 * Performs all relevant checks and stores some information for the allocation.
	 * \param file the file, where the allocation was made
	 * \param line the line in the source, where the allocation was made
	 */
	void* allocate(size_t size, size_t alignment, const char* file, unsigned short line);

private:
	DebugAllocator(const DebugAllocator&);
	DebugAllocator& operator= (const DebugAllocator&);

private:
	class DebugAllocatorImpl;
	DebugAllocatorImpl* _impl;
	// Buffer for the impl
	char _implBuffer[16348];
};

}	// namespace memory
