//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "memory/i_allocator.h"

namespace memory
{

/*!
 * \class RegionAllocator
 * \brief Allocates regions of memory
 * 
 * Allocates a region of memory with a size of \a Size.
 * Then when asked, gives consequtive memory addresses of that region.
 * When the region is depleted, it creates a new region and stores a
 * link to it.
 * No deallocations are done until the layer is destroyed.
 * All of the regions are deallocated in the layer's destructor.
 */
class RegionAllocator : public IAllocator
{
public:
	RegionAllocator(IAllocator& baseAllocator, size_t regionSize);

	/*!
	 * \brief Destroys all allocated regions.
	 */
	virtual ~RegionAllocator();

	/*!
	 * \brief Returns an allocated address.
	 *
	 * Returns the next consequtive address. Allocates a new region if needed.
	 */
	virtual void* allocate(size_t size, size_t alignment);

	/*!
	 * \brief Does not do anything.
	 *
	 * All memory is deallocated on destruction of the layer.
	 */
	virtual void deallocate(void*);

	virtual size_t size(void* ptr);

private:
	RegionAllocator(const RegionAllocator&);
	RegionAllocator& operator= (const RegionAllocator&);

private:
	IAllocator& _baseAllocator;

	struct Region
	{
		Region* next;
		void* currentPtr;
	};
	Region* _currentRegion;

	const size_t _regionSize;
};

}	// namespace memory
