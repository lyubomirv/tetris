//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "memory/i_allocator.h"

namespace memory
{

/*!
 * \class FreeListAllocator
 * \brief Keeps a list of free memory addresses.
 * 
 * When deallocating, it does not actually deallocate but rather
 * embeds a list of free memory spots in the freed addresses.
 * When allocating, it checks if there is an already allocated
 * free spot and returns that. Otherwise it calls the \a SuperLayer
 * for the allocation.
 * \attention All allocations must be of equal size. \todo This can be enforced.
 * \attention A \a base allocator that deallocates all its
 * allocated memory during destruction must be used with
 * the \a FreeListAllocator to avoid memory leaks.
 */
class FreeListAllocator: public IAllocator
{
public:
	FreeListAllocator(IAllocator& baseAllocator);
	virtual ~FreeListAllocator();
	
	/*!
	 * \brief Returns an allocated address.
	 * 
	 * Returns a free address if available. Otherwise uses
	 * the SuperLayer to allocate memory.
	 */
	virtual void* allocate(size_t size, size_t alignment);
	
	/*!
	 * \brief Does not do anything.
	 *
	 * Adds the pointer to the list of free addresses.
	 */
	virtual void deallocate(void* ptr);

	virtual size_t size(void* ptr);
	
	virtual size_t totalSize() const;

private:
	FreeListAllocator(const FreeListAllocator&);
	FreeListAllocator& operator= (const FreeListAllocator&);

private:
	IAllocator& _baseAllocator;

	struct FreeObject
	{
		FreeObject* next;
	};
	
	FreeObject* _freeList;
};

}	// namespace memory
