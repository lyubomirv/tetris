//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "memory/free_list_allocator.h"

namespace memory
{

FreeListAllocator::FreeListAllocator(IAllocator& baseAllocator)
	: _baseAllocator(baseAllocator)
	, _freeList(NULL)
{
}

FreeListAllocator::~FreeListAllocator()
{
}

void* FreeListAllocator::allocate(size_t size, size_t alignment)
{
	void* ptr = _freeList;
	if(ptr == NULL)
	{
		ptr = _baseAllocator.allocate(size, alignment);
	}
	else
	{
		_freeList = _freeList->next;
	}
	
	return ptr;
}

void FreeListAllocator::deallocate(void* ptr)
{
	FreeObject* obj = reinterpret_cast<FreeObject*>(ptr);
	obj->next = _freeList;
	_freeList = obj;
}

size_t FreeListAllocator::size(void* ptr)
{
	return _baseAllocator.size(ptr);
}

size_t FreeListAllocator::totalSize() const
{
	return _baseAllocator.totalSize();
}

}	// namespace memory
