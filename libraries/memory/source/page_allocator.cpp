//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "page_allocator.h"

#include <cassert>
#include <new>

#include "base/platform_defines.h"

#if defined(PLATFORM_LINUX) || defined(PLATFORM_OSX)

#include <unistd.h>
#include <sys/mman.h>

#include "detail/block_metadata.h"

namespace
{
#if defined(PLATFORM_LINUX)
const int MMAP_FLAGS = MAP_PRIVATE | MAP_ANONYMOUS;
#elif defined(PLATFORM_OSX)
const int MMAP_FLAGS = MAP_PRIVATE | MAP_ANON;
#endif	// PLATFORM_LINUX

/**
 * \struct SizeHeader
 * \brief Stored at the beginning of an allocation to track its size.
 */
struct SizeHeader
{
	typedef size_t type;
	type value;
};

/**
 * \brief Gets the actual size for allocating.
 *
 * Counts the required size + padding size for metadata, rounded to the nearest
 * whole system page size.
 */
inline size_t getSizeToAllocate(size_t pageSize, size_t size, size_t alignment)
{
	const size_t sizeWithMetadata = memory::detail::sizeWithPadding<SizeHeader>(size, alignment);
	const size_t totalSize = (sizeWithMetadata / pageSize) * pageSize;
	return (sizeWithMetadata % pageSize != 0)
		? totalSize + pageSize
		: totalSize;
}

}	// namespace

namespace memory
{

PageAllocator::PageAllocator()
	: _systemPageSize(sysconf(_SC_PAGESIZE))
{
}

/*!
 * \brief Allocates pages of memory
 *
 * Calculates what would be the actual size of the allocation -
 * it is a multiple of the system's page size.
 * Alignment is not relevant here, the pages are naturally aligned by
 * a value, that is certainly greater than the supplied one.
 */
void* PageAllocator::allocate(size_t size, size_t alignment)
{
	const size_t realSize = getSizeToAllocate(_systemPageSize, size, alignment);

	SizeHeader* header = static_cast<SizeHeader*>(mmap(
		NULL,
		realSize,
		PROT_READ | PROT_WRITE,
		MMAP_FLAGS,
		-1,
		0));

	if(header == MAP_FAILED)
	{
		assert(false);
	}

	void* ptr = detail::dataPointer(header, alignment);
	detail::fill(header, ptr, realSize);

	onAllocate(realSize);

	return ptr;
}

void PageAllocator::deallocate(void* ptr)
{
	const size_t allocatedSize = size(ptr);
	onDeallocate(allocatedSize);

	SizeHeader* header = detail::headerPointer<SizeHeader>(ptr);
	munmap(header, allocatedSize);
}

size_t PageAllocator::size(void* ptr)
{
	SizeHeader* header = detail::headerPointer<SizeHeader>(ptr);
	return header->value;
}

}	// namespace memory

#elif defined(PLATFORM_WINDOWS)

#include <windows.h>

namespace memory
{

PageAllocator::PageAllocator()
{
	SYSTEM_INFO systemInfo;
	GetSystemInfo(&systemInfo);
	_systemPageSize = systemInfo.dwPageSize;
}

/*!
 * \brief Allocates pages of memory
 *
 * Calculates what would be the actual size of the allocation -
 * it is a multiple of the system's page size.
 * Alignment is not relevant here, the pages are naturally aligned by
 * a value, that is certainly greater than the supplied one.
 */
void* PageAllocator::allocate(size_t size, size_t)
{
	void* ptr = VirtualAlloc(NULL, size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	
	const size_t allocatedSize = this->size(ptr);
	onAllocate(allocatedSize);
	
	return ptr;
}

void PageAllocator::deallocate(void* ptr)
{
	const size_t allocatedSize = size(ptr);
	onDeallocate(allocatedSize);
	
	VirtualFree(ptr, 0, MEM_RELEASE);
}

size_t PageAllocator::size(void* ptr)
{
	MEMORY_BASIC_INFORMATION memoryBasicInformation;
	VirtualQuery(ptr, &memoryBasicInformation, sizeof(MEMORY_BASIC_INFORMATION));
	return static_cast<size_t>(memoryBasicInformation.RegionSize);
}

}	// namespace memory

#endif
