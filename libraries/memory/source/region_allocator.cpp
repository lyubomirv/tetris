//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "memory/region_allocator.h"

#include "detail/block_metadata.h"

namespace memory
{

namespace
{
/**
 * \struct SizeHeader
 * \brief Stored at the beginning of an allocation to track its size.
 */
struct SizeHeader
{
	typedef size_t type;
	type value;
};

/*!
 * \brief Align a pointer to an address, that is the next multiple of \a alignment.
 * \param ptr the original pointer
 * \param alignment the wanted alignment
 * \return the aligned pointer
 */
SizeHeader* align(void* ptr, size_t alignment)
{
	size_t address = reinterpret_cast<size_t>(ptr);
	size_t newAddress = (address + (alignment - 1)) & (~(alignment - 1));
	return reinterpret_cast<SizeHeader*>(newAddress);
}

/*!
 * \brief Tells if there is at least \a size free space in the region,
 * starting from the address, pointed by \a ptr.
 */
bool hasSpace(
	const void* regionStart,
	size_t regionSize,
	void* ptr,
	size_t size)
{
	return static_cast<const char*>(ptr) + size
		<= reinterpret_cast<const char*>(regionStart) + regionSize;
}
}

RegionAllocator::RegionAllocator(
	IAllocator& baseAllocator,
	size_t regionSize)
	: _baseAllocator(baseAllocator)
	, _currentRegion(NULL)
	, _regionSize(regionSize)
{
}

RegionAllocator::~RegionAllocator()
{
	Region* region = _currentRegion;
	while(region != NULL)
	{
		Region* toDelete = region;
		region = region->next;
		_baseAllocator.deallocate(toDelete);

		onDeallocate(_regionSize);
	}
}

void* RegionAllocator::allocate(size_t size, size_t alignment)
{
	const size_t realSize = detail::sizeWithPadding<SizeHeader>(size, alignment);
	SizeHeader* header = (_currentRegion != NULL)
		? align(_currentRegion->currentPtr, alignment)
		: NULL;
	if(header == NULL || !hasSpace(_currentRegion, _regionSize, header, realSize))
	{
		Region* oldRegion = _currentRegion;

		_currentRegion = static_cast<Region*>(
			_baseAllocator.allocate(_regionSize, sizeof(void*)));
		_currentRegion->next = oldRegion;

		onAllocate(_regionSize);

		header = align(reinterpret_cast<char*>(_currentRegion) + sizeof(Region), alignment);
	}

	_currentRegion->currentPtr = reinterpret_cast<char*>(header) + realSize;

	void* realPtr = detail::dataPointer(header, alignment);
	detail::fill(header, realPtr, realSize);

	return realPtr;
}

void RegionAllocator::deallocate(void*)
{
}

size_t RegionAllocator::size(void* ptr)
{
	SizeHeader* header = detail::headerPointer<SizeHeader>(ptr);
	return header->value;
}

}	// namespace memory
