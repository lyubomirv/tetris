//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cxxtest/TestSuite.h>

#include "memory/allocators.h"
#include "memory/memory.h"

class MemoryAllocatorsTest : public CxxTest::TestSuite
{
public:
	void testHeap()
	{
		memory::allocators::create();

		memory::IAllocator& allocator = memory::allocators::heap();
		void* memory = MEMALLOCATE(allocator, int);
		
		int* i = new (memory) int(1);
		
		TS_ASSERT(memory);
		TS_ASSERT_EQUALS(*i, 1);

		TS_ASSERT(allocator.totalSize() >= sizeof(int));
		
		MEMDEALLOCATE(allocator, memory);

		memory::allocators::destroy();
	}

	void testPage()
	{
		memory::allocators::create();

		memory::IAllocator& allocator = memory::allocators::page();
		void* memory = MEMALLOCATE(allocator, int);

		int* i = new (memory) int(1);

		TS_ASSERT(memory);
		TS_ASSERT_EQUALS(*i, 1);

		TS_ASSERT(allocator.totalSize() >= sizeof(int));

		MEMDEALLOCATE(allocator, i);

		memory::allocators::destroy();
	}
};
