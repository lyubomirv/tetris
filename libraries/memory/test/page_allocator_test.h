//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cxxtest/TestSuite.h>

#include "base/platform_defines.h"

#if defined(PLATFORM_LINUX) || defined(PLATFORM_OSX)
#	include <unistd.h>
#elif defined(PLATFORM_WINDOWS)
#	include <windows.h>
#endif

#include "memory/allocators.h"
#include "memory/memory.h"

class PageAllocatorTest : public CxxTest::TestSuite 
{
public:
	virtual void setUp()
	{
		memory::allocators::create();
	}

	virtual void tearDown()
	{
		memory::allocators::destroy();
	}

	PageAllocatorTest()
	{
		#if defined(PLATFORM_LINUX) || defined(PLATFORM_OSX)
			_systemPageSize = sysconf(_SC_PAGESIZE);
		#elif defined(PLATFORM_WINDOWS)
			SYSTEM_INFO systemInfo;
			GetSystemInfo(&systemInfo);
			_systemPageSize = systemInfo.dwPageSize;
		#endif
	}
	
	void testAlloc()
	{
		memory::IAllocator& allocator = memory::allocators::page();
		void* memory = MEMALLOCATE(allocator, int);
		
		int* i = new (memory) int(1);
		
		TS_ASSERT(memory);
		TS_ASSERT_EQUALS(*i, 1);
		
		MEMDEALLOCATE(allocator, i);
	}
	
	void testSizeLessThanOnePage()
	{
		memory::IAllocator& allocator = memory::allocators::page();
		void* memory = MEMALLOCATE(allocator, int);
		
		TS_ASSERT(allocator.size(memory) == _systemPageSize);
		
		MEMDEALLOCATE(allocator, memory);
	}
	
	void testSizeAboutOnePage()
	{
		memory::IAllocator& allocator = memory::allocators::page();
		void* memory = allocator.allocate(_systemPageSize, 8);
		
		TS_ASSERT(allocator.size(memory) >= _systemPageSize);
		
		MEMDEALLOCATE(allocator, memory);
	}
	
	void testSizeMoreThanOnePage()
	{
		memory::IAllocator& allocator = memory::allocators::page();
		void* memory = allocator.allocate(_systemPageSize + 1, 8);
		
		TS_ASSERT(allocator.size(memory) == 2 * _systemPageSize);
		
		MEMDEALLOCATE(allocator, memory);
	}
	
	void testTotalSize()
	{
		memory::IAllocator& allocator = memory::allocators::page();
		void* memory = allocator.allocate(_systemPageSize, 8);
		
		TS_ASSERT(allocator.totalSize() >= _systemPageSize);
		
		MEMDEALLOCATE(allocator, memory);
	}
	
	void testFree()
	{
		memory::IAllocator& allocator = memory::allocators::page();
		void* memory = allocator.allocate(1000000, 8);
		
		allocator.deallocate(memory);
	}

private:
	size_t _systemPageSize;
};
