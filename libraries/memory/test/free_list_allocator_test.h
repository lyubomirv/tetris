//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cxxtest/TestSuite.h>

#include "memory/allocators.h"
#include "memory/free_list_allocator.h"
#include "memory/region_allocator.h"
#include "memory/memory.h"

class FreeListAllocatorTest : public CxxTest::TestSuite 
{
public:
	virtual void setUp()
	{
		memory::allocators::create();
	}

	virtual void tearDown()
	{
		memory::allocators::destroy();
	}

	void testAlloc()
	{
		memory::RegionAllocator regionAllocator(memory::allocators::heap(), 40);
		memory::FreeListAllocator allocator(regionAllocator);
		void* memory = MEMALLOCATE(allocator, int);
		
		int* i = new (memory) int(1);
		
		TS_ASSERT(memory);
		TS_ASSERT_EQUALS(*i, 1);
	}
	
	void testSize()
	{
		memory::RegionAllocator regionAllocator(memory::allocators::heap(), 40);
		memory::FreeListAllocator allocator(regionAllocator);
		void* memory = MEMALLOCATE(allocator, int);
		
		TS_ASSERT(allocator.size(memory) >= sizeof(int));
	}
	
	void testTotalSize()
	{
		memory::RegionAllocator regionAllocator(memory::allocators::heap(), 40);
		memory::FreeListAllocator allocator(regionAllocator);
		MEMALLOCATE(allocator, int);
		
		TS_ASSERT(allocator.totalSize() >= sizeof(int));
	}
	
	void testFree()
	{
		memory::RegionAllocator regionAllocator(memory::allocators::heap(), 40);
		memory::FreeListAllocator allocator(regionAllocator);
		void* memory1 = MEMALLOCATE(allocator, int);
		void* memory2 = MEMALLOCATE(allocator, int);
		void* memory3 = MEMALLOCATE(allocator, int);
		
		allocator.deallocate(memory1);
		allocator.deallocate(memory2);
		
		memory2 = MEMALLOCATE(allocator, int);
		
		allocator.deallocate(memory3);
	}
};
