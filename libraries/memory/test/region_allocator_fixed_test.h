//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cxxtest/TestSuite.h>

#include "memory/allocators.h"
#include "memory/memory.h"
#include "memory/region_allocator_fixed.h"

class RegionAllocatorFixedTest : public CxxTest::TestSuite 
{
public:
	virtual void setUp()
	{
		memory::allocators::create();
	}

	virtual void tearDown()
	{
		memory::allocators::destroy();
	}

	void testAllocHeap()
	{
		memory::RegionAllocatorFixed allocator(memory::allocators::heap(), 40, sizeof(int));
		void* memory = MEMALLOCATE(allocator, int);
		
		int* i = new (memory) int(1);
		
		TS_ASSERT(memory);
		TS_ASSERT_EQUALS(*i, 1);
	}
	
	void testAllocPage()
	{
		memory::RegionAllocatorFixed allocator(memory::allocators::page(), 40, sizeof(int));
		void* memory = MEMALLOCATE(allocator, int);
		
		int* i = new (memory) int(1);
		
		TS_ASSERT(memory);
		TS_ASSERT_EQUALS(*i, 1);
	}
	
	void testSize()
	{
		memory::RegionAllocatorFixed allocator(memory::allocators::heap(), 40, sizeof(int));
		void* memory = MEMALLOCATE(allocator, int);
		
		TS_ASSERT_EQUALS(allocator.size(memory), sizeof(int));
	}
	
	void testTotalSize()
	{
		memory::RegionAllocatorFixed allocator(memory::allocators::heap(), 40, sizeof(int));
		MEMALLOCATE(allocator, int);
		
		TS_ASSERT_EQUALS(allocator.totalSize(), 40);
	}
	
	void testTotalSizeTwoAllocationsBigger()
	{
		memory::RegionAllocatorFixed allocator(memory::allocators::heap(), 40, 5 * sizeof(int));
		allocator.allocate(sizeof(int) * 5, ALIGNOF(int));
		allocator.allocate(sizeof(int) * 5, ALIGNOF(int));
		
		TS_ASSERT_EQUALS(allocator.totalSize(), 80);
	}
	
	void testFree()
	{
		memory::RegionAllocatorFixed allocator(memory::allocators::heap(), 40, 2 * sizeof(int));
		void* memory = allocator.allocate(sizeof(int) * 2, ALIGNOF(int));
		
		allocator.deallocate(memory);
	}
};
