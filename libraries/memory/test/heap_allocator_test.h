//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cxxtest/TestSuite.h>

#include "memory/allocators.h"
#include "memory/memory.h"

class HeapAllocatorTest : public CxxTest::TestSuite 
{
public:
	void testAlloc()
	{
		memory::allocators::create();

		memory::IAllocator& allocator = memory::allocators::heap();
		void* memory = MEMALLOCATE(allocator, int);
		
		int* i = new (memory) int(1);
		
		TS_ASSERT(memory);
		TS_ASSERT_EQUALS(*i, 1);
		
		MEMDEALLOCATE(allocator, i);

		memory::allocators::destroy();
	}
	
	void testAllocNew()
	{
		memory::allocators::create();

		memory::IAllocator& allocator = memory::allocators::heap();
		
		int* i = MEMNEW(allocator, int) (1);
		
		TS_ASSERT(i);
		TS_ASSERT_EQUALS(*i, 1);
		
		MEMDELETE(allocator, i);

		memory::allocators::destroy();
	}
	
	void testSize()
	{
		memory::allocators::create();

		memory::IAllocator& allocator = memory::allocators::heap();
		void* memory = MEMALLOCATE(allocator, int);
		
		TS_ASSERT(allocator.size(memory) >= sizeof(int));
		
		MEMDEALLOCATE(allocator, memory);

		memory::allocators::destroy();
	}
	
	void testTotalSize()
	{
		memory::allocators::create();

		memory::IAllocator& allocator = memory::allocators::heap();
		void* memory = MEMALLOCATE(allocator, int);
		
		TS_ASSERT(allocator.totalSize() >= sizeof(int));
		
		MEMDEALLOCATE(allocator, memory);

		memory::allocators::destroy();
	}
	
	void testFree()
	{
		memory::allocators::create();

		memory::IAllocator& allocator = memory::allocators::heap();
		void* memory = MEMALLOCATE(allocator, int);
		
		MEMDEALLOCATE(allocator, memory);

		memory::allocators::destroy();
	}
};
