//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cxxtest/TestSuite.h>

#include "memory/allocators.h"
#include "memory/../../source/detail/hash_map.h"

// A custom hash function
namespace memory
{
namespace detail
{
template<>
inline size_t hash(int i)
{
	return static_cast<size_t>(i);
}
}
}

class HashMapTest : public CxxTest::TestSuite 
{
public:
	virtual void setUp()
	{
		memory::allocators::create();
	}

	virtual void tearDown()
	{
		memory::allocators::destroy();
	}

	void testCreate()
	{
		memory::detail::HashMap<int, int, 10> hashMap(memory::allocators::heap());
		
		TS_ASSERT(true);
		TS_ASSERT_EQUALS(1, 1);
	}
	
	void testHasEmpty()
	{
		memory::detail::HashMap<int, int, 10> hashMap(memory::allocators::heap());
		
		TS_ASSERT(!hashMap.has(1));
	}
	
	void testAddOne()
	{
		memory::detail::HashMap<int, int, 10> hashMap(memory::allocators::heap());
		hashMap.set(1, 21);
		
		TS_ASSERT(hashMap.has(1));
	}
	
	void testAddTwoColliding()
	{
		memory::detail::HashMap<int, int, 10> hashMap(memory::allocators::heap());
		hashMap.set(1, 21);
		hashMap.set(11, 33);
		
		TS_ASSERT(hashMap.has(1));
		TS_ASSERT(hashMap.has(11));
	}
	
	void testAddFour()
	{
		memory::detail::HashMap<int, int, 10> hashMap(memory::allocators::heap());
		hashMap.set(123, 456);
		hashMap.set(234, 567);
		hashMap.set(1, 21);
		hashMap.set(11, 33);
		
		TS_ASSERT(hashMap.has(123));
		TS_ASSERT(hashMap.has(234));
		TS_ASSERT(hashMap.has(1));
		TS_ASSERT(hashMap.has(11));
	}
	
	void testHasNotPresent()
	{
		memory::detail::HashMap<int, int, 10> hashMap(memory::allocators::heap());
		hashMap.set(1, 123);
		
		TS_ASSERT(!hashMap.has(2));
	}
	
	void testValueOne()
	{
		memory::detail::HashMap<int, int, 10> hashMap(memory::allocators::heap());
		hashMap.set(1, 123);
		
		TS_ASSERT_EQUALS(hashMap.get(1), 123);
	}
	
	void testValueTwoColliding()
	{
		memory::detail::HashMap<int, int, 10> hashMap(memory::allocators::heap());
		hashMap.set(1, 123);
		hashMap.set(11, 456);
		
		TS_ASSERT_EQUALS(hashMap.get(1), 123);
		TS_ASSERT_EQUALS(hashMap.get(11), 456);
	}
	
	void testValueFour()
	{
		memory::detail::HashMap<int, int, 10> hashMap(memory::allocators::heap());
		hashMap.set(1, 123);
		hashMap.set(11, 456);
		hashMap.set(13450, 111235);
		hashMap.set(-1, 213);
		
		TS_ASSERT_EQUALS(hashMap.get(1), 123);
		TS_ASSERT_EQUALS(hashMap.get(11), 456);
		TS_ASSERT_EQUALS(hashMap.get(13450), 111235);
		TS_ASSERT_EQUALS(hashMap.get(-1), 213);
	}
	
	void testRemoveEmpty()
	{
		memory::detail::HashMap<int, int, 10> hashMap(memory::allocators::heap());
		hashMap.erase(15);
		
		TS_ASSERT(!hashMap.has(15));
	}
	
	void testRemoveNotPresent()
	{
		memory::detail::HashMap<int, int, 10> hashMap(memory::allocators::heap());
		hashMap.set(1, 123);
		hashMap.erase(15);
		
		TS_ASSERT(hashMap.has(1));
		TS_ASSERT(!hashMap.has(15));
	}
	
	void testRemove()
	{
		memory::detail::HashMap<int, int, 10> hashMap(memory::allocators::heap());
		hashMap.set(1, 123);
		
		TS_ASSERT(hashMap.has(1));
		
		hashMap.erase(1);
		
		TS_ASSERT(!hashMap.has(1));
	}
	
	void testEmpty()
	{
		memory::detail::HashMap<int, int, 10> hashMap(memory::allocators::heap());
		
		TS_ASSERT(hashMap.empty());
	}
	
	void testNotEmpty()
	{
		memory::detail::HashMap<int, int, 10> hashMap(memory::allocators::heap());
		hashMap.set(1, 123);
		
		TS_ASSERT(!hashMap.empty());
	}
	
	void testIteratorEmpty()
	{
		typedef memory::detail::HashMap<int, int, 10> HashMap10;
		HashMap10 hashMap(memory::allocators::heap());
		
		HashMap10::const_iterator beginIt = hashMap.begin();
		HashMap10::const_iterator endIt = hashMap.end();
		
		TS_ASSERT_EQUALS(beginIt, endIt);
	}
	
	void testIterateOne()
	{
		typedef memory::detail::HashMap<int, int, 10> HashMap10;
		HashMap10 hashMap(memory::allocators::heap());
		
		hashMap.set(1, 123);
		
		HashMap10::const_iterator it = hashMap.begin();
		++it;
		TS_ASSERT_EQUALS(it, hashMap.end());
	}
	
	void testIterate()
	{
		typedef memory::detail::HashMap<int, int, 10> HashMap10;
		HashMap10 hashMap(memory::allocators::heap());
		
		hashMap.set(1, 123);
		
		for(HashMap10::const_iterator it = hashMap.begin(); it != hashMap.end(); ++it)
		{
			TS_ASSERT_EQUALS(it.key(), 1);
			TS_ASSERT_EQUALS(it.value(), 123);
		}
	}
};
