//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cxxtest/TestSuite.h>

#include "memory/allocators.h"
#include "memory/memory.h"
#include "memory/region_allocator.h"

class RegionAllocatorTest : public CxxTest::TestSuite 
{
public:
	virtual void setUp()
	{
		memory::allocators::create();
	}

	virtual void tearDown()
	{
		memory::allocators::destroy();
	}

	void testAllocHeap()
	{
		memory::RegionAllocator allocator(memory::allocators::heap(), 40);
		void* memory = MEMALLOCATE(allocator, int);
		
		int* i = new (memory) int(1);
		
		TS_ASSERT(memory);
		TS_ASSERT_EQUALS(*i, 1);
	}
	
	void testAllocPage()
	{
		memory::RegionAllocator allocator(memory::allocators::page(), 40);
		void* memory = MEMALLOCATE(allocator, int);
		
		int* i = new (memory) int(1);
		
		TS_ASSERT(memory);
		TS_ASSERT_EQUALS(*i, 1);
	}
	
	void testSize()
	{
		memory::RegionAllocator allocator(memory::allocators::heap(), 40);
		void* memory = MEMALLOCATE(allocator, int);
		
		TS_ASSERT(allocator.size(memory) >= sizeof(int));
	}
	
	void testTotalSize()
	{
		memory::RegionAllocator allocator(memory::allocators::heap(), 40);
		MEMALLOCATE(allocator, int);
		
		TS_ASSERT(allocator.totalSize() >= 40);
	}
	
	void testTotalSizeTwoAllocationsBigger()
	{
		memory::RegionAllocator allocator(memory::allocators::heap(), 40);
		allocator.allocate(sizeof(int) * 5, ALIGNOF(int));
		allocator.allocate(sizeof(int) * 5, ALIGNOF(int));
		
		TS_ASSERT(allocator.totalSize() >= 80);
	}
	
	void testFree()
	{
		memory::RegionAllocator allocator(memory::allocators::heap(), 40);
		void* memory = allocator.allocate(sizeof(int) * 2, ALIGNOF(int));
		
		allocator.deallocate(memory);
	}
};
