//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

namespace collection
{

/**
 * \struct ObjectIdType
 * \brief A unique identifier for an object in a subsystem.
 *
 * An object is uniquely identified by an id of this type. The id consists of an
 * index in the \a indices collection of the \a ObjectManager (\a index_type)
 * and a unique identity of the object (\a identity_type), that distinguishes it
 * from any other object that may have been indexed with the same identifier
 * (the \a identity is unique for each object in each index position).
 * The structure allows to define those types in order to be flexible for use in
 * subsystems with different requirements for the objects amount and
 * insertion/removal frequency.
 * \tparam IdentityType type to use for the object \a identity.
 * \tparam IndexType type to use for the \a index in the \a indices collection.
 */
template <typename IdentityType, typename IndexType>
struct ObjectIdType
{
	typedef IdentityType identity_type;
	typedef IndexType index_type;

	bool operator== (const ObjectIdType& other) const
	{
		return identity == other.identity && index == other.index;
	}

	bool operator!= (const ObjectIdType& other) const
	{
		return !operator==(other);
	}

	identity_type identity;
	index_type index;
};

}	// namespace collection
