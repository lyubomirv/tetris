//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <cxxtest/TestSuite.h>

#include "mstd/map.h"

#include "memory/allocators.h"

class MapTest : public CxxTest::TestSuite 
{
public:
	virtual void setUp()
	{
		memory::allocators::create();
	}

	virtual void tearDown()
	{
		memory::allocators::destroy();
	}

	void testCreateDefault()
	{
		mstd::Map<int, int> m(memory::allocators::heap());
		
		TS_ASSERT(m.empty());
	}
	
	void testInsert()
	{
		mstd::Map<int, int> m(memory::allocators::heap());
		
		m.insert(mstd::Map<int, int>::value_type(1, 2));
		
		TS_ASSERT_EQUALS(m.size(), 1);
	}
	
	void testCreateIterators()
	{
		mstd::Map<int, int> m1(memory::allocators::heap());
		m1.insert(mstd::Map<int, int>::value_type(1, 2));
		m1.insert(mstd::Map<int, int>::value_type(2, 3));
		m1.insert(mstd::Map<int, int>::value_type(3, 4));
		
		mstd::Map<int, int> m2(memory::allocators::heap(), m1.begin(), m1.end());
		
		TS_ASSERT_EQUALS(m2.size(), 3);
	}
	
	void testCopyConstruct()
	{
		mstd::Map<int, int> m1(memory::allocators::heap());
		m1.insert(mstd::Map<int, int>::value_type(1, 1));
		m1.insert(mstd::Map<int, int>::value_type(2, 1));
		m1.insert(mstd::Map<int, int>::value_type(3, 1));
		
		mstd::Map<int, int> m2(m1);
		
		TS_ASSERT_EQUALS(m2.size(), 3);
		
		for(mstd::Map<int, int>::const_iterator it = m2.begin(); it != m2.end(); ++it)
		{
			int value = it->second;
			TS_ASSERT_EQUALS(value, 1);
		}
	}
	
	void testCopyConstructWithAnotherAllocator()
	{
		mstd::Map<int, int> m1(memory::allocators::heap());
		m1.insert(mstd::Map<int, int>::value_type(1, 1));
		m1.insert(mstd::Map<int, int>::value_type(2, 1));
		m1.insert(mstd::Map<int, int>::value_type(3, 1));
		
		mstd::Map<int, int> m2(memory::allocators::heap(), m1);
		
		TS_ASSERT_EQUALS(m2.size(), 3);
		
		for(mstd::Map<int, int>::const_iterator it = m2.begin(); it != m2.end(); ++it)
		{
			int value = it->second;
			TS_ASSERT_EQUALS(value, 1);
		}
	}
	
	void testAssignmentOperator()
	{
		mstd::Map<int, int> m1(memory::allocators::heap());
		m1.insert(mstd::Map<int, int>::value_type(1, 1));
		m1.insert(mstd::Map<int, int>::value_type(2, 1));
		m1.insert(mstd::Map<int, int>::value_type(3, 1));
		
		mstd::Map<int, int> m2(memory::allocators::heap());
		m2 = m1;
		
		TS_ASSERT_EQUALS(m2.size(), 3);
		
		for(mstd::Map<int, int>::const_iterator it = m2.begin(); it != m2.end(); ++it)
		{
			int value = it->second;
			TS_ASSERT_EQUALS(value, 1);
		}
	}
	
	void testStructConstruct()
	{
		mstd::Map<int, IntWrapper> m(memory::allocators::heap());
		m.insert(mstd::Map<int, IntWrapper>::value_type(1, IntWrapper()));
		m.insert(mstd::Map<int, IntWrapper>::value_type(2, IntWrapper()));
		m.insert(mstd::Map<int, IntWrapper>::value_type(3, IntWrapper()));
		
		TS_ASSERT_EQUALS(m.size(), 3);
		
		for(mstd::Map<int, IntWrapper>::const_iterator it = m.begin(); it != m.end(); ++it)
		{
			const IntWrapper& wrapper = it->second;
			TS_ASSERT_EQUALS(wrapper.wrapped, 1);
		}
	}
	
	void testStructKeyOperatorLess()
	{
		mstd::Map<IntWrapper, int> m(memory::allocators::heap());
		m.insert(mstd::Map<IntWrapper, int>::value_type(IntWrapper(1), 1));
		m.insert(mstd::Map<IntWrapper, int>::value_type(IntWrapper(2), 1));
		m.insert(mstd::Map<IntWrapper, int>::value_type(IntWrapper(3), 1));
		
		TS_ASSERT_EQUALS(m.size(), 3);
		
		for(mstd::Map<IntWrapper, int>::const_iterator it = m.begin(); it != m.end(); ++it)
		{
			int value = it->second;
			TS_ASSERT_EQUALS(value, 1);
		}
	}
	
	void testStructKeyFunctor()
	{
		typedef mstd::Map<IntWrapper, int, IntWrapperComparator> Map_t;
		Map_t m(memory::allocators::heap(), IntWrapperComparator());
		m.insert(Map_t::value_type(IntWrapper(1), 1));
		m.insert(Map_t::value_type(IntWrapper(2), 1));
		m.insert(Map_t::value_type(IntWrapper(3), 1));
		
		TS_ASSERT_EQUALS(m.size(), 3);
		
		for(Map_t::const_iterator it = m.begin(); it != m.end(); ++it)
		{
			int value = it->second;
			TS_ASSERT_EQUALS(value, 1);
		}
	}

private:
	struct IntWrapper
	{
		IntWrapper()
			: wrapped(1)
		{
		}
		
		IntWrapper(int i)
			: wrapped(i)
		{
		}
		
		bool operator< (const IntWrapper& other) const
		{
			return wrapped < other.wrapped;
		}
		
		int wrapped;
	};
	
	struct IntWrapperComparator
	{
		bool operator() (const IntWrapper& left, const IntWrapper& right) const
		{
			return left.wrapped < right.wrapped;
		}
	};
};
