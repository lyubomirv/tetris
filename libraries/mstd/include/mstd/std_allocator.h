//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include <limits>

#include "memory/i_allocator.h"
#include "memory/memory.h"

namespace mstd
{

// Forward declaration
template <class T> class StdAllocator;
 
// Specialization for void
template <> class StdAllocator<void>
{
public:
	typedef void* pointer;
	typedef const void* const_pointer;

	// Reference to void members are impossible.

	typedef void value_type;
	template <class U> struct rebind
	{
		typedef StdAllocator<U> other;
	};
};

/*!
 * \class StdAllocator
 * \brief An implementation of an STL allocator.
 *
 * This allocator is a proxy to use with the stl.
 * It uses an allocator from 'memory' to do the actual allocations.
 * \tparam T type of the objects that this allocator will allocate
 */
template <class T>
class StdAllocator
{
public:
	// Type definitions
	typedef T        value_type;
	typedef T*       pointer;
	typedef const T* const_pointer;
	typedef T&       reference;
	typedef const T& const_reference;
	typedef std::size_t    size_type;
	typedef std::ptrdiff_t difference_type;

	/*!
	 * \struct rebind
	 * \brief Rebinds StdAllocator to type \a U.
	 */
	template <class U>
	struct rebind
	{
		typedef StdAllocator<U> other;
	};

public:
	StdAllocator(memory::IAllocator& allocator) throw()
		: _allocator(&allocator)
	{
	}
	
	StdAllocator(const StdAllocator& other) throw()
		: _allocator(other._allocator)
	{
	}
	
	template <class U>
	StdAllocator(const StdAllocator<U>& other) throw()
		: _allocator(other.getAllocator())
	{
	}
	
	~StdAllocator() throw()
	{
	}

	/*!
	 * \brief Returns the address of \a value.
	 */
	pointer address(reference value) const
	{
		return &value;
	}
	
	/*!
	 * \brief Returns the address of the const \a value.
	 */
	const_pointer address(const_reference value) const
	{
		return &value;
	}

	/*!
	 * \brief Allocates but does not initialize \a count elements of type T.
	 */
	pointer allocate(size_type count, const StdAllocator<void>::const_pointer = 0)
	{
		return static_cast<pointer>(_allocator->allocate(sizeof(T) * count, ALIGNOF(T)));
	}
	
	/*!
	 * \brief Deallocates the storage for the memory, pointed by \a ptr.
	 */
	void deallocate(pointer ptr, size_type)
	{
		_allocator->deallocate(ptr);
	}
	
	/*!
	 * \brief Returns the maximum number of elements that can be allocated.
	 */
	size_type max_size() const throw()
	{
		return std::numeric_limits<size_type>::max() / sizeof(T);
	}

	/*!
	 * \brief Initializes the allocated memory, pointed by \a ptr with value \a value.
	 *
	 * Calls placement new on the allocated memory.
	 */
	void construct(pointer ptr, const T& value)
	{
		new(ptr) T(value);
	}

	/*!
	 * \brief Destroys the allocated memory, pointed by \a ptr.
	 *
	 * Calls the destructor of the class T.
	 */
	void destroy(pointer ptr)
	{
		memory::detail::destructType(ptr);
	}
	
	/*!
	 * \brief Assigns another StdAllocator.
	 * \note This method is needed only for the MSVC compiler.
	 * Otherwise it is not part of the standart allocator interface.
	 */
	StdAllocator<T>& operator= (const StdAllocator<T>& other)
	{
		if(this != &other)
		{
			_allocator = other._allocator;
		}
		
		return *this;
	}

public:
	memory::IAllocator* getAllocator() const
	{
		return _allocator;
	}

private:
	memory::IAllocator* _allocator;
};

// Returns that all specializations of this allocator are interchangeable
template <class T1, class T2>
bool operator== (const StdAllocator<T1>&, const StdAllocator<T2>&) throw()
{
	return true;
}

// Returns that all specializations of this allocator are interchangeable
template <class T1, class T2>
bool operator!= (const StdAllocator<T1>&, const StdAllocator<T2>&) throw()
{
	return false;
}

}	// namespace mstd
