tetris
------

A tetris game. This repository is a backup of that game's code, so that it doesn't get lost.

The code uses another project of mine as a base - https://gitlab.com/lyubomirv/libraries but it already has a copy of those libraries, for easy setup.

The game does not have any interface at the moment. It used to have, when it was implemented only for windows, but since the code became multi-platform, that was lost. It may be introduced once again in the future.

### Setting up development environment

Linux/OSX:

* Install g++;
* Install cmake;
* Install python

Windows:

* Install Microsoft Windows SDK 7.1;
* Install Python;
* Install CMake.

### Building

To build the game and the test executables, you need to:

    mkdir build
    cd build
    cmake ..
    make
When you need to compile for 32bit architecture, you need to add to the `cmake ..` command an additional argument: `-DARCHITECTURE_BITS_64=OFF`.

If you then want to run the tests:

    make test
