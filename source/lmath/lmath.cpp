#include "precompiled.h"

#include "lmath/lmath.h"

#include "lmath/vector3.h"

namespace lmath
{

const float PI = 3.14159265358979323846;

inline float abs(float x)
{
	return (x >= 0.0f) ? x : -x;
}

bool Alike(float a, float b, float epsilon)
{
	return abs(a - b) < epsilon;
}

bool Alike(const Vector3& a, const Vector3& b, float epsilon)
{
	if(abs(a.x - b.x) > epsilon)
		return false;
	if(abs(a.y - b.y) > epsilon)
		return false;
	//if(abs(a.z - b.z) > epsilon)
	//	return false;
	
	return true;
}

Vector3 Rotate(float angle, const Vector3& v)
{
	if(angle < 0 && !Alike(angle, 0.0f))
		angle += 360.0f;
	
	if(Alike(angle, 0.0f))
		return v;
	
	Vector3 temp = v;
	
	if(Alike(angle, 90.0f))
	{
		if(Alike(v.y, 0.0f))
		{
			temp.x = v.y;
			temp.y = v.x;
		}
		else
		{
			temp.x = -v.y;
			temp.y = v.x;
		}
		return temp;
	}
	
	if(Alike(angle, 180.0f))
	{
		temp.x = -v.x;
		temp.y = -temp.y;
		return temp;
	}
	
	if(Alike(angle, 270.0f))
	{
		if(Alike(v.x, 0.0f))
		{
			temp.x = v.y;
			temp.y = v.x;
		}
		else
		{
			temp.x = v.y;
			temp.y = -v.x;
		}
		return temp;
	}
	
	return v;
}

}	// namespace lmath
