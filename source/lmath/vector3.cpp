#include "precompiled.h"

#include "lmath/vector3.h"

#include <cmath>

Vector3::Vector3()
	: x(0.0f)
	, y(0.0f)
	, z(0.0f)
{
}

Vector3::Vector3(float _x, float _y, float _z)
	: x(_x)
	, y(_y)
	, z(_z)
{
}

Vector3::Vector3(const Vector3& other)
{
	x = other.x;
	y = other.y;
	z = other.z;
}

Vector3& Vector3::operator= (const Vector3& other)
{
	x = other.x;
	y = other.y;
	z = other.z;
	return *this;
}

const Vector3 Vector3::operator+ (const Vector3& other)
{
	Vector3 temp = *this;
	temp.x += other.x;
	temp.y += other.y;
	temp.z += other.z;
	return temp;
}

const Vector3 Vector3::operator- (const Vector3& other)
{
	Vector3 temp = *this;
	temp.x -= other.x;
	temp.y -= other.y;
	temp.z -= other.z;
	return temp;
}

const Vector3 Vector3::operator* (float coef)
{
	Vector3 temp = *this;
	temp.x *= coef;
	temp.y *= coef;
	temp.z *= coef;
	return temp;
}

float Vector3::operator* (const Vector3& other)
{
	return x * other.x + y * other.y + z * other.z;
}

Vector3& Vector3::operator+= (const Vector3& other)
{
	x += other.x;
	y += other.y;
	z += other.z;
	return *this;
}

Vector3& Vector3::operator-= (const Vector3& other)
{
	x -= other.x;
	y -= other.y;
	z -= other.z;
	return *this;
}

Vector3& Vector3::operator*= (float coef)
{
	x *= coef;
	y *= coef;
	z *= coef;
	return *this;
}

void Vector3::normalize()
{
	float division = sqrt(x * x + y * y + z * z);
	
	if(division == 0.0)
		return;
		
	x /= division;
	y /= division;
	z /= division;
}
