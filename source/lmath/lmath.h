#pragma once

struct Vector3;

namespace lmath
{
	extern const float PI;
	float abs(float x);
	bool Alike(float a, float b, float epsilon = 0.1f);
	bool Alike(const Vector3& a, const Vector3& b, float epsilon = 0.1f);
	Vector3 Rotate(float angle, const Vector3& v);
}
