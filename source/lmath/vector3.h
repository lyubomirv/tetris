#pragma once

struct Vector3
{
	Vector3();
	Vector3(float _x, float _y, float _z);
	Vector3(const Vector3& other);
	
	Vector3& operator= (const Vector3& other);
	
	const Vector3 operator+ (const Vector3& other);
	const Vector3 operator- (const Vector3& other);
	
	const Vector3 operator* (float coef);
	float operator* (const Vector3& other);
	
	Vector3& operator+= (const Vector3& other);
	Vector3& operator-= (const Vector3& other);
	
	Vector3& operator*= (float coef);
	
	void normalize();
	
	float x;
	float y;
	float z;
};
