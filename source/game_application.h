#pragma once

#include "engine/application.h"

class Quad;
class IShape;
class Playground;
class Menu;

class GameApplication : public Application
{
public:
	GameApplication();
	~GameApplication();

protected:
	virtual bool initialize();
	virtual void handleInput(float deltaTimeSeconds);

public:
	virtual void onResize();
	virtual void onDraw(float deltaTime);

private:
	void createObjects();
	void destroyObjects();

	void startNewGame();
	bool applyGravity(float timePassed);

	void processCommand(const std::string& command);

	void calculateScore(int newScore);
	int loadHighScore(const std::string& filename);
	void setHighScore(const std::string& filename, int score);

	IShape* createRandomShape();

private:
	input::InputSystem::ContextId _globalContextId;
	input::InputSystem::ContextId _gameContextId;
	input::InputSystem::ContextId _menuContextId;

private:
	IShape* m_CurrentShape;
	IShape* m_NextShape;

	Playground* m_Playground;

	Menu* m_Menu;

	int m_Score;
	int m_RowsCleared;
	int m_Level;

	int m_PreviousMenu;

	bool m_Done;
	bool m_Suspended;

	float m_TimePassed;
};
