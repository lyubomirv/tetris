#include "precompiled.h"

#include "playground/playground.h"

#include <sstream>

#include "shapes/i_shape.h"

Playground::Playground()
	: m_Width(Settings::PlaygroundWidth * Settings::QuadSide)
	, m_Height(Settings::PlaygroundHeight * Settings::QuadSide)
	, m_Center()
{
	Initialize();
}

Playground::Playground(const Vector3& position)
	: m_Width(Settings::PlaygroundWidth * Settings::QuadSide)
	, m_Height(Settings::PlaygroundHeight * Settings::QuadSide)
	, m_Center(position)
{
	Initialize();
}

void Playground::Initialize()
{
	// Place Quads around the Playground
	Vector3 pos = m_Center;
	pos.x = pos.x - m_Width / 2 + Settings::QuadSide / 2;
	pos.y = pos.y + m_Height / 2 + Settings::QuadSide / 2;
	
	for(int i = 0; i < Settings::PlaygroundWidth - 1; ++i)
	{
		m_Surrounding.push_back(pos);
		pos.x += Settings::QuadSide;
	}
	
	pos.x += Settings::QuadSide;
	pos.y -= Settings::QuadSide;
	
	for(int i = 0; i < Settings::PlaygroundHeight; ++i)
	{
		m_Surrounding.push_back(pos);
		pos.y -= Settings::QuadSide;
	}
	
	pos.x -= Settings::QuadSide;
	
	for(int i = 0; i < Settings::PlaygroundWidth; ++i)
	{
		m_Surrounding.push_back(pos);
		pos.x -= Settings::QuadSide;
	}
	
	pos.y += Settings::QuadSide;
	
	for(int i = 0; i < Settings::PlaygroundHeight; ++i)
	{
		m_Surrounding.push_back(pos);
		pos.y += Settings::QuadSide;
	}
	
	// Initialize the vectors
	Vector3 startPosition = m_Center;
	startPosition.x = startPosition.x - m_Width / 2 + Settings::QuadSide / 2;
	startPosition.y = startPosition.y - m_Height / 2 + Settings::QuadSide / 2;
	
	m_BoolField.resize(Settings::PlaygroundHeight, std::vector<bool>(Settings::PlaygroundWidth, false));
	m_VectorField.resize(Settings::PlaygroundHeight);
	for(size_t i = 0; i < m_VectorField.size(); ++i)
	{
		m_VectorField[i].resize(Settings::PlaygroundWidth);
		for(size_t j = 0; j < m_VectorField[i].size(); ++j)
		{
			m_VectorField[i][j] = Vector3(startPosition.x + j * Settings::QuadSide,
											startPosition.y + i * Settings::QuadSide,
											startPosition.z);
		}
	}
	
	m_ColorField.resize(Settings::PlaygroundHeight, std::vector<Vector3>(Settings::PlaygroundWidth));
}

//void Playground::InitializeTexts(GLuint base)
//{
//	m_TextNext = Text(base, Vector3(m_Center.x - m_Width / 2 - Settings::QuadSide * 5 + 0.4f, m_Center.y + m_Height / 2 - 2.2f, m_Center.z));
//	m_TextNext.SetText("��������");
//	
//	m_TextScore = Text(base, Vector3(m_Center.x + m_Width / 2 + Settings::QuadSide + 0.4f, m_Center.y + m_Height / 2 - 2.2f, m_Center.z));
//	m_TextScore.SetText("�����");
//	
//	m_TextScoreNumber = Text(base, Vector3(m_Center.x + m_Width / 2 + Settings::QuadSide + 0.4f, m_Center.y + m_Height / 2 - 4.0f, m_Center.z));
//	m_TextScoreNumber.SetText("0");
//	
//	m_TextLevel = Text(base, Vector3(m_Center.x + m_Width / 2 + Settings::QuadSide + 0.4f, m_Center.y + m_Height / 2 - 7.2f, m_Center.z));
//	m_TextLevel.SetText("����");
//	
//	m_TextLevelNumber = Text(base, Vector3(m_Center.x + m_Width / 2 + Settings::QuadSide + 0.4f, m_Center.y + m_Height / 2 - 9.0f, m_Center.z));
//	m_TextLevelNumber.SetText("1");
//	
//	m_TextHighScore = Text(base, Vector3(m_Center.x + m_Width / 2 + Settings::QuadSide + 0.4f, m_Center.y + m_Height / 2 - 12.2f, m_Center.z));
//	m_TextHighScore.SetText("���-����� ��������");
//	
//	m_TextHighScoreNumber = Text(base, Vector3(m_Center.x + m_Width / 2 + Settings::QuadSide + 0.4f, m_Center.y + m_Height / 2 - 14.0f, m_Center.z));
//	m_TextHighScoreNumber.SetText("0");
//}

void Playground::Draw()
{
	// Drawing Quads
	// Draw Quads first so that the border lines are above the Quads
	for(size_t i = 0; i < m_VectorField.size(); ++i)
	{
		for(size_t j = 0; j < m_VectorField[i].size(); ++j)
		{
			if(m_BoolField[i][j] == true)
			{
				m_Quad.SetPosition(m_VectorField[i][j]);
				m_Quad.SetColor(m_ColorField[i][j]);
				glMatrixMode(GL_MODELVIEW);
				glLoadIdentity();
				m_Quad.Draw();
			}
		}
	}
	
	// Draw Texts
	//m_TextNext.Draw();
	//m_TextScore.Draw();
	//m_TextScoreNumber.Draw();
	//m_TextLevel.Draw();
	//m_TextLevelNumber.Draw();
	//m_TextHighScore.Draw();
	//m_TextHighScoreNumber.Draw();
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	Vector3 pos = m_Center;
	pos.x -= m_Width / 2;
	pos.y += m_Height / 2;
	
	// Color of the Playground
	glColor3f(1.0f, 1.0f, 1.0f);
	
	glBegin(GL_LINE_STRIP);
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(float(m_Width), 0.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos -= Vector3(0.0f, float(m_Height), 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos -= Vector3(float(m_Width), 0.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(0.0f, float(m_Height), 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
	glEnd();
	
	
	// Draw the place for the Next shape
	glBegin(GL_LINE_STRIP);
		pos += Vector3(-Settings::QuadSide * 5, -2.0f, 0.0f);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(0.2f, 0.0f, 0.0f);//Settings::QuadSide * 4, 0.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
	glEnd();
	
	glBegin(GL_LINE_STRIP);
		pos += Vector3(-0.2f + Settings::QuadSide * 3 + 0.9f, 0.0f, 0.0f);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(-0.9f + Settings::QuadSide, 0.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(0.0f, -Settings::QuadSide * 5, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(-Settings::QuadSide * 4, 0.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(0.0f, Settings::QuadSide * 5, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
	glEnd();
	
	// Draw the place for the score
	pos += Vector3(Settings::QuadSide * 6 + float(m_Width), 0.0f, 0.0f);
	
	glBegin(GL_LINE_STRIP);
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(0.2f, 0.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
	glEnd();
	
	glBegin(GL_LINE_STRIP);
		pos += Vector3(-0.2f + Settings::QuadSide * 2, 0.0f, 0.0f);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(Settings::QuadSide * 4, 0.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(0.0f, -3.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(-Settings::QuadSide * 6, 0.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(0.0f, 3.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
	glEnd();
	
	//Draw the place for the level
	pos += Vector3(0.0f, -5.0f, 0.0f);
	
	glBegin(GL_LINE_STRIP);
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(0.2f, 0.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
	glEnd();
	
	glBegin(GL_LINE_STRIP);
		pos += Vector3(-0.2f + Settings::QuadSide * 2 - 0.3f, 0.0f, 0.0f);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(0.3f + Settings::QuadSide * 4, 0.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(0.0f, -3.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(-Settings::QuadSide * 6, 0.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(0.0f, 3.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
	glEnd();
	
	// Draw the place for the highscore
	pos += Vector3(0.0f, -5.0f, 0.0f);
	
	glBegin(GL_LINE_STRIP);
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(0.2f, 0.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
	glEnd();
	
	glBegin(GL_LINE_STRIP);
		pos += Vector3(-0.2f + Settings::QuadSide * 6, 0.0f, 0.0f);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(0.0f, 0.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(0.0f, -3.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(-Settings::QuadSide * 6, 0.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		pos += Vector3(0.0f, 3.0f, 0.0f);
		glVertex3f(pos.x, pos.y, pos.z);
	glEnd();
	
	glLoadIdentity();
}

bool Playground::CanMove(IShape* shape, const Vector3& direction, float dist)
{
	Vector3 dir = direction;
	dir = dir * dist;
	Vector3 shapePos = dir + shape->GetPosition();
	
	IShape::QuadList::const_iterator it = shape->GetQuads().begin();
	for(; it != shape->GetQuads().end(); ++it)
	{
		Vector3 quadPos = shapePos + (*it)->GetPosition();
		
		/*VectorList::const_iterator vectIt = m_Surrounding.begin();
		for(vectIt; vectIt != m_Surrounding.end(); ++vectIt)
		{
			if(lmath::Alike(quadPos, (*vectIt)))
			{
				return false;
			}
		}*/
		
		//if(quadPos.y > m_Center.y + m_Height / 2)
		//	return false;
		if(quadPos.y < m_Center.y - m_Height / 2)
			return false;
		if(quadPos.x < m_Center.x - m_Width / 2)
			return false;
		if(quadPos.x > m_Center.x + m_Width / 2)
			return false;
		
		for(size_t i = 0; i < m_VectorField.size(); ++i)
		{
			for(size_t j = 0; j < m_VectorField[i].size(); ++j)
			{
				if(m_BoolField[i][j] == true)
				{
					if(lmath::Alike(quadPos, m_VectorField[i][j]))
						return false;
				}
			}
		}
	}
	
	return true;
}

bool Playground::CanRotate(IShape* shape, float angle, Vector3& directionToMove)
{
	Vector3 shapePos = shape->GetPosition();
	
	IShape::QuadList::const_iterator it = shape->GetQuads().begin();
	std::vector<Vector3> quads;
	for(; it != shape->GetQuads().end(); ++it)
	{
		Vector3 quadPos = (*it)->GetPosition();
		quadPos = lmath::Rotate(angle, quadPos);
		quads.push_back(shapePos + quadPos);
	}
	
	int firstDirection = 0;
	int direction = 0;
	
	std::vector<Vector3>::iterator quadIt = quads.begin();
	for(; quadIt != quads.end(); ++quadIt)
	{
		VectorList::const_iterator vectIt = m_Surrounding.begin();
		for(; vectIt != m_Surrounding.end(); ++vectIt)
		{
			if(lmath::Alike((*quadIt), (*vectIt)))
			{
				firstDirection = (shapePos.x < vectIt->x) ? -1 : 1;
				if(lmath::Alike(shapePos.x, vectIt->x))
				{
					firstDirection = (shapePos.y < vectIt->y) ? -2 : 2;
					break;
				}
			}
		}
		
		for(size_t i = 0; i < m_VectorField.size(); ++i)
		{
			for(size_t j = 0; j < m_VectorField[i].size(); ++j)
			{
				if(m_BoolField[i][j] == true)
				{
					if(lmath::Alike((*quadIt), m_VectorField[i][j]))
					{
						firstDirection = (shapePos.x <= m_VectorField[i][j].x) ? -1 : 1;
					}
				}
			}
		}
	}
	
	if(firstDirection == 0)
	{
		directionToMove = Vector3();
		return true;
	}
	
	direction = firstDirection;
	Vector3 dir;
	Vector3 directionWithDist;
	
	if(firstDirection == -2 || firstDirection == 2)
		dir = Vector3(0.0f, (float(firstDirection) / 2) * Settings::MoveSpeed, 0.0f);
	else
		dir = Vector3(float(direction) * Settings::MoveSpeed, 0.0f, 0.0f);
	
	while(direction == firstDirection)
	{
		directionWithDist = directionWithDist + dir;
		std::vector<Vector3>::iterator q = quads.begin();
		for(; q != quads.end(); ++q)
		{
			(*q) = (*q) + dir;
		}
		
		q = quads.begin();
		bool shouldMove = false;
		for(; q != quads.end(); ++q)
		{
			VectorList::const_iterator v = m_Surrounding.begin();
			for(; v != m_Surrounding.end(); ++v)
			{
				if(lmath::Alike((*q), (*v)))
				{
					direction = (shapePos.x < v->x) ? -1 : 1;
					if(lmath::Alike(shapePos.x, v->x))
					{
						direction = (shapePos.y < v->y) ? -2 : 2;
					}
					shouldMove = true;
				}
			}
			
			for(size_t i = 0; i < m_VectorField.size(); ++i)
			{
				for(size_t j = 0; j < m_VectorField[i].size(); ++j)
				{
					if(m_BoolField[i][j] == true)
					{
						if(lmath::Alike((*q), m_VectorField[i][j]))
						{
							direction = (shapePos.x < m_VectorField[i][j].x) ? -1 : 1;
							shouldMove = true;
						}
					}
				}
			}
		}
		
		if(!shouldMove)
			direction = 0;
	}
	
	if(direction == 0)
	{
		directionToMove = directionWithDist;
		return true;
	}
	else
	{
		return false;
	}
}

bool Playground::AppendShape(IShape* shape)
{
	Vector3 bottomLeft = m_Center;
	bottomLeft.x = bottomLeft.x - m_Width / 2 + Settings::QuadSide / 2;
	bottomLeft.y = bottomLeft.y - m_Height / 2 + Settings::QuadSide / 2;
	
	Vector3 shapePos = shape->GetPosition();
	Vector3 quadPos;
	size_t i = 0, j = 0;
	IShape::QuadList::const_iterator it = shape->GetQuads().begin();
	for(; it != shape->GetQuads().end(); ++it)
	{
		quadPos = shapePos + (*it)->GetPosition();
		i = int((quadPos.y - bottomLeft.y) / 2);
		j = int((quadPos.x - bottomLeft.x) / 2);
		
		if(i >= m_BoolField.size() || j >= m_BoolField[0].size())
		{
			return false;
		}
		
		m_BoolField[i][j] = true;
		m_ColorField[i][j] = (*it)->GetColor();
	}
	
	return true;
}

Vector3 Playground::GetStartPosition()
{
	int index = Settings::PlaygroundWidth / 2;
	Vector3 startPos = m_Surrounding[index];
	startPos.y -= Settings::QuadSide;
	
	return startPos;
}

Vector3 Playground::GetNextPosition(int index)
{
	Vector3 pos = m_Center;
	pos.x = pos.x - m_Width / 2 - Settings::QuadSide * 5;
	pos.y = pos.y + m_Height / 2 - 2.0f;
	
	switch(index)
	{
		case 0:
			pos += Vector3(Settings::QuadSide * 1.5f, -Settings::QuadSide * 3, 0.0f);
			break;
		case 1:
			pos += Vector3(Settings::QuadSide * 2.5f, -Settings::QuadSide * 3, 0.0f);
			break;
		case 2:
			pos += Vector3(Settings::QuadSide * 2, -Settings::QuadSide * 2, 0.0f);
			break;
		case 3:
			pos += Vector3(Settings::QuadSide * 1.5, -Settings::QuadSide * 3, 0.0f);
			break;
		case 4:
			pos += Vector3(Settings::QuadSide * 1.5f, -Settings::QuadSide * 3, 0.0f);
			break;
		case 5:
			pos += Vector3(Settings::QuadSide * 2.5f, -Settings::QuadSide * 3, 0.0f);
			break;
		case 6:
			pos += Vector3(Settings::QuadSide * 2, -Settings::QuadSide * 4, 0.0f);
			break;
	}
	
	return pos;
}

bool Playground::IsRowFilled(size_t index)
{
	for(size_t j = 0; j < m_BoolField[index].size(); ++j)
		if(m_BoolField[index][j] == false)
			return false;
	
	return true;
}

int Playground::ClearRows()
{
	int count = 0;
	
	for(size_t i = 0; i < m_BoolField.size(); ++i)
	{
		if(IsRowFilled(i))
		{
			for(size_t j = i; j < m_BoolField.size() - 1; ++j)
			{
				m_BoolField[j] = m_BoolField[j + 1];
				m_ColorField[j] = m_ColorField[j + 1];
			}
			m_BoolField.back() = std::vector<bool>(m_BoolField[0].size(), false);
			
			--i;
			++count;
		}
	}
	
	return count;
}

void Playground::SetScore(int score)
{
	std::ostringstream ss;
	ss << score;
	//m_TextScoreNumber.SetText(ss.str());
}

void Playground::SetLevel(int level)
{
	std::ostringstream ss;
	ss << level;
	//m_TextLevelNumber.SetText(ss.str());
}

void Playground::SetHighScore(int highScore)
{
	std::ostringstream ss;
	ss << highScore;
	//m_TextHighScoreNumber.SetText(ss.str());
}
