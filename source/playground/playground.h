#pragma once

#include "shapes/quad.h"
//#include "text/text.h"

class IShape;

class Playground
{
public:
	typedef std::vector<Vector3> VectorList;
	typedef std::vector<std::vector<bool> > BoolMatrix;
	typedef std::vector<std::vector<Vector3> > Vector3Matrix;
	typedef std::vector<std::vector<Vector3> > ColorMatrix;
	
	Playground();
	Playground(const Vector3& position);
	
	//void InitializeTexts(GLuint base);
	
	int GetWidth() const;
	int GetHeight() const;
	Vector3 GetCenter() const;
	const VectorList& GetSurrounding() const;
	
	void Draw();
	
	bool CanMove(IShape* shape, const Vector3& direction, float dist);
	bool CanRotate(IShape* shape, float angle, Vector3& directionToMove);
	
	bool AppendShape(IShape* shape);
	
	Vector3 GetStartPosition();
	Vector3 GetNextPosition(int index);
	int ClearRows();
	
	void SetScore(int score);
	void SetLevel(int level);
	void SetHighScore(int highScore);
	
private:
	void Initialize();
	bool IsRowFilled(size_t index);
	
	int m_Width;
	int m_Height;
	Vector3 m_Center;
	
	VectorList m_Surrounding;
	BoolMatrix m_BoolField;
	Vector3Matrix m_VectorField;
	ColorMatrix m_ColorField;
	Quad m_Quad;
	
	// Texts
	//Text m_TextNext;
	//Text m_TextScore;
	//Text m_TextScoreNumber;
	//Text m_TextLevel;
	//Text m_TextLevelNumber;
	//Text m_TextHighScore;
	//Text m_TextHighScoreNumber;
};

inline int Playground::GetWidth() const
{
	return m_Width;
}

inline int Playground::GetHeight() const
{
	return m_Height;
}

inline Vector3 Playground::GetCenter() const
{
	return m_Center;
}

inline const Playground::VectorList& Playground::GetSurrounding() const
{
	return m_Surrounding;
}
