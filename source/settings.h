#pragma once

#include <string>

#include "lmath/vector3.h"

namespace Settings
{
	static const int ApplicationWidth = 800;
	static const int ApplicationHeight = 600;
	static std::string ApplicationName = "Lyubo Tetris";
	static const int QuadSide = 2;
	static const float CameraZ = -60.0f;
	static const int PlaygroundWidth = 11;
	static const int PlaygroundHeight = 21;
	static const int MoveSpeed = 2;
	static const Vector3 QuadColor = Vector3(0.0f, 0.0f, 1.0f);
	static const int ShapeTypesCount = 7;
}
