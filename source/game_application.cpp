#include "precompiled.h"

#include "game_application.h"

#include <cstdio>
#include <cmath>

#include "input/input.h"

#include "shapes/quad.h"
#include "shapes/i_shape.h"
#include "shapes/shape_l.h"
#include "shapes/shape_g.h"
#include "shapes/shape_i.h"
#include "shapes/shape_o.h"
#include "shapes/shape_z.h"
#include "shapes/shape_s.h"
#include "shapes/shape_t.h"
//#include "text/text.h"
#include "playground/playground.h"
#include "menu/menu.h"

using namespace game;

/////////////////////////////////////////////////////////////
// Helpers

/*!
 * \brief Sets the open gl perspective.
 *
 * Uses glFrustum.
 * \note Source: http://steinsoft.net/index.php?site=Programming/Code%20Snippets/OpenGL/gluperspective
 */
static void setGLPerspective(
	GLdouble fovy,
	GLdouble aspect,
	GLdouble zNear,
	GLdouble zFar)
{
	GLdouble xmin, xmax, ymin, ymax;

	ymax = zNear * tan(fovy * lmath::PI / 360.0);
	ymin = -ymax;
	xmin = ymin * aspect;
	xmax = ymax * aspect;

	glFrustum(xmin, xmax, ymin, ymax, zNear, zFar);
}

/////////////////////////////////////////////////////////////
// GameApplication

GameApplication::GameApplication()
	: Application(
		Settings::ApplicationWidth,
		Settings::ApplicationHeight,
		Settings::ApplicationName.c_str())
	, m_Playground(NULL)
	, m_Menu(NULL)
	, m_Score(0)
	, m_RowsCleared(0)
	, m_Level(0)
	, m_PreviousMenu(0)
	, m_Done(false)
	, m_Suspended(true)
	, m_TimePassed(0.0f)
{
}

GameApplication::~GameApplication()
{
	destroyObjects();
}

bool GameApplication::initialize()
{
	_globalContextId = createGlobalContext(_input);
	_gameContextId = createGameContext(_input);
	_menuContextId = createMenuContext(_input);
	initializeInputSystem(_input);
	_input.setContextActive(_globalContextId);

	srand(unsigned(time(NULL)));

	// Rendering
	glShadeModel(GL_SMOOTH);	// Enables Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f);			// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);	// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);		// The Type Of Depth Test To Do

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
	// End rendering

	createObjects();

	//CreateText(16);
	//m_Playground->InitializeTexts(m_Base);
	m_Playground->SetHighScore(loadHighScore("highscore.hsc"));
	if(m_Menu)
		delete m_Menu;
	m_Menu = new Menu(Menu::MenuType(m_PreviousMenu));

	_input.setContextActive(_menuContextId);

	return true;
}

void GameApplication::handleInput(float)
{
	const input::Action* actions = _input.getActions();
	for(size_t i = 0; i < _input.getActionsCount(); ++i)
	{
		const input::Action& action = actions[i];
		switch(action.id)
		{
			case GAME_MOVE_SHAPE_LEFT:
				{
					Vector3 direction(-1.0f, 0.0f, 0.0f);
					if(m_Playground->CanMove(m_CurrentShape, direction, float(Settings::MoveSpeed)))
						m_CurrentShape->Move(direction, float(Settings::MoveSpeed));
				}
				break;
			case GAME_MOVE_SHAPE_RIGHT:
				{
					Vector3 direction(1.0f, 0.0f, 0.0f);
					if(m_Playground->CanMove(m_CurrentShape, direction, float(Settings::MoveSpeed)))
						m_CurrentShape->Move(direction, float(Settings::MoveSpeed));
				}
				break;
			case GAME_MOVE_SHAPE_DOWN:
				{
					Vector3 direction(0.0f, -1.0f, 0.0f);
					if(m_Playground->CanMove(m_CurrentShape, direction, float(Settings::MoveSpeed)))
						m_CurrentShape->Move(direction, float(Settings::MoveSpeed));
				}
				break;
			case GAME_ROTATE_SHAPE_LEFT:
				{
					Vector3 direction;
					if(m_Playground->CanRotate(m_CurrentShape, 90.0f, direction))
					{
						m_CurrentShape->RotateLeft();
						m_CurrentShape->Move(direction, 1.0f);
					}
				}
				break;
			case GAME_ROTATE_SHAPE_RIGHT:
				{
					Vector3 direction;
					if(m_Playground->CanRotate(m_CurrentShape, -90.0f, direction))
					{
						m_CurrentShape->RotateRight();
						m_CurrentShape->Move(direction, 1.0f);
					}
				}
				break;
			case GAME_PAUSE:
				{
					m_Suspended = true;
					if(m_Menu)
						delete m_Menu;
					m_Menu = new Menu(Menu::InGame);
					m_PreviousMenu = 1;

					_input.setContextInactive(_gameContextId);
					_input.setContextActive(_menuContextId);
				}
				break;
			case MENU_UNPAUSE:
				{
					if(!m_Menu->back())
					{
						m_Suspended = false;
						delete m_Menu;
						m_Menu = NULL;

						_input.setContextInactive(_menuContextId);
						_input.setContextActive(_gameContextId);
					}
				}
				break;
			case MENU_UP:
				m_Menu->goUp();
				break;
			case MENU_DOWN:
				m_Menu->goDown();
				break;
			case MENU_SELECT:
				m_Menu->select();
				break;

			case GLOBAL_FULLSCREEN:
				requestModeChange(!isFullscreen());
				break;
		}
	}
}

void GameApplication::onResize()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Calculate The Aspect Ratio Of The Window
	setGLPerspective(45.0f, (GLfloat)width()/(GLfloat)height(), 0.1f, 100.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void GameApplication::onDraw(float deltaTime)
{
	m_TimePassed += deltaTime;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();		// Reset The Current Modelview Matrix

	if(!m_Suspended)
	{
		if(!applyGravity(m_TimePassed))
		{
			if(m_Playground->AppendShape(m_CurrentShape))
			{
				int newScore = m_Playground->ClearRows();
				calculateScore(newScore);

				delete m_CurrentShape;
				m_CurrentShape = m_NextShape;
				m_CurrentShape->SetPosition(m_Playground->GetStartPosition());
				m_NextShape = createRandomShape();
			}
			else
			{
				int highScore = loadHighScore("highscore.hsc");
				if(m_Score > highScore)
					setHighScore("highscore.hsc", m_Score);

				m_Suspended = true;
				m_Menu = new Menu(Menu::Start);
				m_PreviousMenu = 0;
			}
		}
	}

	m_CurrentShape->Draw();
	m_NextShape->Draw();

	m_Playground->Draw();

	std::string command;
	if(m_Suspended)
	{
		if(m_Menu->GetCommand(command))
		{
			delete m_Menu;
			m_Suspended = false;
			m_Menu = NULL;
			_input.setContextInactive(_menuContextId);
			_input.setContextActive(_gameContextId);
			processCommand(command);
		}
		else
			m_Menu->Draw();
	}
}

void GameApplication::createObjects()
{
	if(m_Playground)
	{
		delete m_Playground;
	}

	m_Playground = new Playground(Vector3(0.0f, 0.0f, Settings::CameraZ));

	m_NextShape = createRandomShape();
	m_CurrentShape = createRandomShape();
	m_CurrentShape->SetPosition(m_Playground->GetStartPosition());
}

void GameApplication::destroyObjects()
{
	if(m_CurrentShape)
	{
		delete m_CurrentShape;
		m_CurrentShape = NULL;
	}

	delete m_Playground;
	m_Playground = NULL;
}

void GameApplication::startNewGame()
{
	if(m_Playground)
		delete m_Playground;
	if(m_NextShape)
		delete m_NextShape;
	if(m_CurrentShape)
		delete m_CurrentShape;

	m_Score = 0;

	createObjects();
	//m_Playground->InitializeTexts(m_Base);
	m_Suspended = false;
}

bool GameApplication::applyGravity(float timePassed)
{
	static float lastTime = timePassed;

	float currTime = timePassed;

	float checkRate = 0.0;

	switch(m_Level)
	{
		case 0:
			checkRate = 0.4f;
			break;
		case 1:
			checkRate = 0.3f;
			break;
		case 2:
			checkRate = 0.25f;
			break;
		case 3:
			checkRate = 0.2f;
			break;
		case 4:
			checkRate = 0.17f;
			break;
		case 5:
			checkRate = 0.15f;
			break;
		case 6:
			checkRate = 0.12f;
			break;
		case 7:
			checkRate = 0.1f;
			break;
		case 8:
			checkRate = 0.08f;
			break;
		case 9:
			checkRate = 0.06f;
			break;
		default:
			checkRate = 0.05f;
	}

	if(currTime - lastTime >= checkRate)
	{
		lastTime = currTime;

		Vector3 direction(0.0f, -1.0f, 0.0f);
		if(m_Playground->CanMove(m_CurrentShape, direction, float(Settings::MoveSpeed)))
		{
			m_CurrentShape->Move(direction, float(Settings::MoveSpeed));
			return true;
		}
		else
		{
			return false;
		}
	}

	return true;
}

void GameApplication::processCommand(const std::string& command)
{
	if(command == "Ïðîäúëæè èãðàòà")
		return;
	if(command == "Íîâà Èãðà")
	{
		startNewGame();
		return;
	}
	if(command == "Óïðàâëåíèå")
	{
		m_Suspended = true;
		m_Menu = new Menu(Menu::Controls);
	}
	else if(command == "Çà èãðàòà")
	{
		m_Suspended = true;
		m_Menu = new Menu(Menu::About);
	}
	else if(command == "Èçõîä")
	{
		int highScore = loadHighScore("highscore.hsc");
		if(m_Score > highScore)
			setHighScore("highscore.hsc", m_Score);

		m_Done = true;
		return;
	}
	else if(command == "Íàçàä")
	{
		m_Suspended = true;
		m_Menu = new Menu(Menu::MenuType(m_PreviousMenu));
	}
}

void GameApplication::calculateScore(int newScore)
{
	if((m_RowsCleared + newScore) / 25 > m_RowsCleared / 25)
	{
		++m_Level;
		m_Playground->SetLevel(m_Level + 1);
	}

	m_RowsCleared += newScore;

	if(m_Score + newScore > m_Score)
	{
		m_Score += 10 * newScore;
		if(newScore > 1)
			m_Score += 5 * newScore - 5;
		m_Playground->SetScore(m_Score);
	}
}

int GameApplication::loadHighScore(const std::string& filename)
{
	FILE* fin = fopen(filename.c_str(), "rb");

	int score = 0;

	if(fin == NULL)
	{
		fin = fopen(filename.c_str(), "wb");
		fwrite(&score, 1, sizeof(int), fin);
		fclose(fin);
		return 0;
	}

	fread(&score, 1, sizeof(int), fin);
	fclose(fin);

	return score;
}

void GameApplication::setHighScore(const std::string& filename, int score)
{
	FILE* fout = fopen(filename.c_str(), "wb");

	if(fout == NULL)
		return;

	fwrite(&score, 1, sizeof(int), fout);
	fclose(fout);
}

IShape* GameApplication::createRandomShape()
{
	IShape* newShape = NULL;

	int index = rand() % Settings::ShapeTypesCount;

	switch(index)
	{
		case 0:
		{
			newShape = new ShapeL(m_Playground->GetNextPosition(0));
			break;
		}
		case 1:
		{
			newShape = new ShapeG(m_Playground->GetNextPosition(1));
			break;
		}
		case 2:
		{
			newShape = new ShapeI(m_Playground->GetNextPosition(2));
			break;
		}
		case 3:
		{
			newShape = new ShapeO(m_Playground->GetNextPosition(3));
			break;
		}
		case 4:
		{
			newShape = new ShapeZ(m_Playground->GetNextPosition(4));
			break;
		}
		case 5:
		{
			newShape = new ShapeS(m_Playground->GetNextPosition(5));
			break;
		}
		case 6:
		{
			newShape = new ShapeT(m_Playground->GetNextPosition(6));
			break;
		}
	}

	newShape->Initialize();

	return newShape;
}
