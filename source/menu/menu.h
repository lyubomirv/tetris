#pragma once

//#include "text/text.h"

#include <SFML/Window/Keyboard.hpp>

class Menu
{
public:
	enum MenuType
	{
		Start,
		InGame,
		About,
		Controls
	};

public:
	Menu(MenuType menuType, const Vector3& position = Vector3(-6.0f, 6.0f, Settings::CameraZ + 1.0f));
	
	void Draw();
	
	bool GetCommand(std::string& command);

	void goUp();
	void goDown();
	void select();
	bool back();
	
	int GetType() const;

private:
	//std::vector<Text> m_TextsItems;
	int m_SelectedIndex;
	
	bool m_HasCommand;
	int m_MenuType;
	
	//Text m_TextTitle;
	//std::vector<Text> m_TextInformation;
	
	Vector3 m_Position;
	Vector3 m_UnselectedColor;
	Vector3 m_SelectedColor;
};

inline int Menu::GetType() const
{
	return m_MenuType;
}
