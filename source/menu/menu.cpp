#include "precompiled.h"

#include "menu/menu.h"

Menu::Menu(MenuType menuType, const Vector3& position)
	: m_SelectedIndex(0)
	, m_HasCommand(false)
	, m_MenuType(menuType)
	, m_Position(position)
	, m_UnselectedColor(Vector3(1.0f, 1.0f, 1.0f))
	, m_SelectedColor(Vector3(1.0f, 1.0f, 0.0f))
{
	if(m_MenuType == Menu::About || m_MenuType == Menu::Controls)
		m_Position.x -= 2.0f;
	Vector3 pos = m_Position;
	pos.z += 0.5f;
	
	if(m_MenuType == Menu::InGame)
	{
		pos.x += 1.0f;
		pos.y -= 4.0f;
		//m_TextsItems.push_back(Text(base, pos));
		//m_TextsItems.back().SetText("Continue game");
	}
	
	pos = m_Position;
	pos.x += 3.0f;
	pos.z += 0.5f;
	if(m_MenuType == 0)
		pos.y -= 4.0f;
	else
		pos.y -= 6.0f;
	if(m_MenuType == Menu::Start || m_MenuType == Menu::InGame)
	{
		//m_TextsItems.push_back(Text(base, pos));
		//m_TextsItems.back().SetText("New game");
	}
	
	if(m_MenuType == Menu::Start || m_MenuType == Menu::InGame)
	{
		pos.x -= 0.5f;
		pos.y -= 2.0f;
		//m_TextsItems.push_back(Text(base, pos));
		//m_TextsItems.back().SetText("Controls");
		
		pos.x += 0.5f;
		pos.y -= 2.0f;
		//m_TextsItems.push_back(Text(base, pos));
		//m_TextsItems.back().SetText("About");
		
		pos.x += 1.2f;
		pos.y -= 2.0f;
		//m_TextsItems.push_back(Text(base, pos));
		//m_TextsItems.back().SetText("Exit");
	}
	
	if(m_MenuType == Menu::About)
	{
		pos = m_Position;
		pos.x += 1.0f;
		pos.y -= 4.0f;
		pos.z += 0.5f;
		//m_TextInformation.push_back(Text(base, pos));
		//m_TextInformation.back().SetText("");
		
		pos.y -= 2.0f;
		//m_TextInformation.push_back(Text(base, pos));
		//m_TextInformation.back().SetText("");
		
		pos.x += 5.0f;
		pos.y -= 2.0f;
		//m_TextsItems.push_back(Text(base, pos));
		//m_TextsItems.back().SetText("");
	}
	
	if(m_MenuType == Menu::Controls)
	{
		pos = m_Position;
		pos.x += 1.0f;
		pos.y -= 4.0f;
		pos.z += 0.5f;
		//m_TextInformation.push_back(Text(base, pos));
		//m_TextInformation.back().SetText("");
		
		pos.y -= 2.0f;
		//m_TextInformation.push_back(Text(base, pos));
		//m_TextInformation.back().SetText("");
		
		pos.y -= 2.0f;
		//m_TextInformation.push_back(Text(base, pos));
		//m_TextInformation.back().SetText("");
		
		pos.y -= 2.0f;
		//m_TextInformation.push_back(Text(base, pos));
		//m_TextInformation.back().SetText("");
		
		pos.x += 5.0f;
		pos.y -= 2.0f;
		//m_TextsItems.push_back(Text(base, pos));
		//m_TextsItems.back().SetText("");
	}
	
	pos = m_Position;
	pos.x += 4.3f;
	pos.y -= 1.2f;
	pos.z += 0.5f;
	if(m_MenuType == Menu::Start || m_MenuType == Menu::InGame)
	{
		//m_TextTitle = Text(base, pos);
		//m_TextTitle.SetText("");
	}
	else if(m_MenuType == Menu::About)
	{
		pos.x += 1.0f;
		//m_TextTitle = Text(base, pos);
		//m_TextTitle.SetText("");
	}
	else if(m_MenuType == Menu::Controls)
	{
		pos.x += 1.0f;
		//m_TextTitle = Text(base, pos);
		//m_TextTitle.SetText("");
	}
}

void Menu::Draw()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	Vector3 pos = m_Position;
	glColor3f(0.5f, 0.5f, 1.0f);
	
	// Main part
	glBegin(GL_TRIANGLE_STRIP);
		glVertex3f(pos.x, pos.y, pos.z);
		
		pos.x += 12.0f;
		if(m_MenuType == Menu::About || m_MenuType == Menu::Controls)
			pos.x += 4.0f;
		glVertex3f(pos.x, pos.y, pos.z);
		
		pos.y -= 12.0f;
		if(m_MenuType == Menu::InGame || m_MenuType == Menu::Controls)
			pos.y -= 2.0f;
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		
		pos.x -= 12.0f;
		if(m_MenuType == Menu::About || m_MenuType == Menu::Controls)
			pos.x -= 4.0f;
		glVertex3f(pos.x, pos.y, pos.z);
		
		pos.y += 12.0f;
		if(m_MenuType == Menu::InGame || m_MenuType == Menu::Controls)
			pos.y += 2.0f;
		glVertex3f(pos.x, pos.y, pos.z);
	glEnd();
	
	// Title part
	pos = m_Position;
	glColor3f(0.5f, 0.5f, 0.5f);
	
	glBegin(GL_TRIANGLE_STRIP);
		glVertex3f(pos.x, pos.y, pos.z);
		
		pos.x += 12.0f;
		if(m_MenuType == Menu::About || m_MenuType == Menu::Controls)
			pos.x += 4.0f;
		glVertex3f(pos.x, pos.y, pos.z);
		
		pos.y -= 1.5f;
		glVertex3f(pos.x, pos.y, pos.z);
		
		glVertex3f(pos.x, pos.y, pos.z);
		
		pos.x -= 12.0f;
		if(m_MenuType == Menu::About || m_MenuType == Menu::Controls)
			pos.x -= 4.0f;
		glVertex3f(pos.x, pos.y, pos.z);
		
		pos.y += 1.5f;
		glVertex3f(pos.x, pos.y, pos.z);
	glEnd();
	
	/*
	for(size_t i = 0; i < m_TextsItems.size(); ++i)
	{
		if(m_SelectedIndex == i)
		{
			m_TextsItems[i].SetColor(m_SelectedColor);
		}
		else
		{
			m_TextsItems[i].SetColor(m_UnselectedColor);
		}
		m_TextsItems[i].Draw();
	}
	*/
	
	//m_TextTitle.Draw();
	//for(size_t i = 0; i < m_TextInformation.size(); ++i)
	//	m_TextInformation[i].Draw();
}

bool Menu::GetCommand(std::string& /*command*/)
{
	if(!m_HasCommand)
		return false;
	
	//command = m_TextsItems[m_SelectedIndex].GetText();
	return true;
}

void Menu::goUp()
{
	--m_SelectedIndex;
}

void Menu::goDown()
{
	++m_SelectedIndex;
}

void Menu::select()
{
	m_HasCommand = true;
}

bool Menu::back()
{
	return m_MenuType != Menu::About;
}
