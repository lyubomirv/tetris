//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "input/input_system.h"

namespace game
{

enum Actions
{
	GLOBAL_FULLSCREEN,

	GAME_MOVE_SHAPE_LEFT,
	GAME_MOVE_SHAPE_RIGHT,
	GAME_MOVE_SHAPE_DOWN,
	GAME_ROTATE_SHAPE_LEFT,
	GAME_ROTATE_SHAPE_RIGHT,
	GAME_PAUSE,

	MENU_UP,
	MENU_DOWN,
	MENU_SELECT,
	MENU_UNPAUSE
};

input::InputSystem::ContextId createGlobalContext(input::InputSystem& inputSystem);
input::InputSystem::ContextId createGameContext(input::InputSystem& inputSystem);
input::InputSystem::ContextId createMenuContext(input::InputSystem& inputSystem);
void initializeInputSystem(input::InputSystem& inputSystem);

}	// namespace game
