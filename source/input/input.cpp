//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "input/input.h"

namespace game
{

input::InputSystem::ContextId createGlobalContext(input::InputSystem& inputSystem)
{
	input::Context context;
	context.addMapping(input::keyboard::F1, GLOBAL_FULLSCREEN);

	return inputSystem.addContext(context);
}

input::InputSystem::ContextId createGameContext(input::InputSystem& inputSystem)
{
	input::Context context;
	context.addMapping(input::keyboard::Left  , GAME_MOVE_SHAPE_LEFT);
	context.addMapping(input::keyboard::Right , GAME_MOVE_SHAPE_RIGHT);
	context.addMapping(input::keyboard::Down  , GAME_MOVE_SHAPE_DOWN);
	context.addMapping(input::keyboard::D     , GAME_ROTATE_SHAPE_LEFT);
	context.addMapping(input::keyboard::F     , GAME_ROTATE_SHAPE_RIGHT);
	context.addMapping(input::keyboard::Escape, GAME_PAUSE);

	return inputSystem.addContext(context);
}

input::InputSystem::ContextId createMenuContext(input::InputSystem& inputSystem)
{
	input::Context context;
	context.addMapping(input::keyboard::Up    , MENU_UP);
	context.addMapping(input::keyboard::Down  , MENU_DOWN);
	context.addMapping(input::keyboard::Return, MENU_SELECT);
	context.addMapping(input::keyboard::Escape, MENU_UNPAUSE);

	return inputSystem.addContext(context);
}

void initializeInputSystem(input::InputSystem& inputSystem)
{
	inputSystem.registerAction(GLOBAL_FULLSCREEN);

	inputSystem.registerRepeatedAction(GAME_MOVE_SHAPE_LEFT, 150);
	inputSystem.registerRepeatedAction(GAME_MOVE_SHAPE_RIGHT, 150);
	inputSystem.registerRepeatedAction(GAME_MOVE_SHAPE_DOWN, 40);

	inputSystem.registerAction(GAME_ROTATE_SHAPE_LEFT);
	inputSystem.registerAction(GAME_ROTATE_SHAPE_RIGHT);

	inputSystem.registerAction(GAME_PAUSE);

	inputSystem.registerAction(MENU_UP);
	inputSystem.registerAction(MENU_DOWN);
	inputSystem.registerAction(MENU_SELECT);
	inputSystem.registerAction(MENU_UNPAUSE);
}

}	// namespace game
