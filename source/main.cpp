//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include <iostream>

#include "game_application.h"

int main()
{
	GameApplication gameApplication;
	gameApplication.run();
	
	std::cout << "Hi there" << std::endl;
	return 0;
}
