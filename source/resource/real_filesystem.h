#pragma once

#include "resource/i_filesystem.h"
#include "memory/i_allocator.h"
#include "mstd/vector.h"

namespace resource
{

class RealFilesystem : public IFilesystem
{
public:
	RealFilesystem(memory::IAllocator& allocator);

public:
	virtual FileId open(const char* filename, const char* mode);
	virtual void close(FileId fileId);
	virtual bool readAll(FileId fileId, mstd::Vector<unsigned char>& outBuffer);
	virtual bool readAll(FileId fileId, mstd::Vector<char>& outBuffer);

private:
	typedef mstd::Vector<FileId> FileIds;
	FileIds _fileIds;
};

}	// namespace resource
