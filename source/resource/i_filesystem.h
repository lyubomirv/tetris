#pragma once

#include <cstdlib>

#include "mstd/vector.h"

namespace resource
{

class IFilesystem
{
public:
	typedef size_t FileId;
	static const FileId InvalidFileId = FileId(-1);

public:
	/**
	 * \brief Open a file.
	 * \param mode Matches the mode parameter in the standard fopen() function.
	 */
	virtual FileId open(const char* filename, const char* mode) = 0;
	virtual void close(FileId fileId) = 0;
	virtual bool readAll(FileId fileId, mstd::Vector<unsigned char>& outBuffer) = 0;
	virtual bool readAll(FileId fileId, mstd::Vector<char>& outBuffer) = 0;
};
}	// namespace resource
