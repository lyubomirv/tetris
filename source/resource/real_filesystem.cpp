#include "resource/real_filesystem.h"

#include <cstdio>

namespace resource
{

namespace
{
static size_t length(FILE* file)
{
	const long int pos = ftell(file);

	fseek(file, 0, SEEK_END);
	const long int fileSize = ftell(file);
	fseek(file, pos, SEEK_SET);

	return fileSize;
}
}

RealFilesystem::RealFilesystem(memory::IAllocator& allocator)
	: _fileIds(allocator)
{
}

IFilesystem::FileId RealFilesystem::open(const char* filename, const char* mode)
{
	FILE* file = fopen(filename, mode);
	if(!file)
	{
		return IFilesystem::InvalidFileId;
	}

	_fileIds.push_back(reinterpret_cast<FileId>(file));
	return _fileIds.back();
}

void RealFilesystem::close(FileId fileId)
{
	FILE* file = reinterpret_cast<FILE*>(fileId);
	fclose(file);
}

bool RealFilesystem::readAll(IFilesystem::FileId fileId, mstd::Vector<unsigned char>& outBuffer)
{
	FILE* file = reinterpret_cast<FILE*>(fileId);

	outBuffer.resize(length(file));
	const size_t bytesRead = fread(&outBuffer[0], sizeof(unsigned char), outBuffer.size(), file);
	return bytesRead == outBuffer.size();
}

bool RealFilesystem::readAll(IFilesystem::FileId fileId, mstd::Vector<char>& outBuffer)
{
	FILE* file = reinterpret_cast<FILE*>(fileId);

	outBuffer.resize(length(file));
	const size_t bytesRead = fread(&outBuffer[0], sizeof(char), outBuffer.size(), file);
	return bytesRead == outBuffer.size();
}

}	// namespace resource
