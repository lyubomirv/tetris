#pragma once

#include <vector>

#include "lmath/vector3.h"

class Quad
{
public:
	Quad();
	Quad(const Vector3& position, const Vector3& color = Vector3(1.0f, 1.0f, 1.0f));
	
	void Draw();
	
	void Move(Vector3& direction, float dist);
	
	Vector3 GetPosition() const;
	void SetPosition(const Vector3& position);
	Vector3 GetColor() const;
	void SetColor(const Vector3& color);
private:
	void InitializeVertices();
	
	Vector3 m_Position;
	std::vector<Vector3> m_Vertices;
	
	Vector3 m_Color;
};

inline Vector3 Quad::GetPosition() const
{
	return m_Position;
}

inline void Quad::SetPosition(const Vector3& position)
{
	m_Position = position;
}

inline Vector3 Quad::GetColor() const
{
	return m_Color;
}

inline void Quad::SetColor(const Vector3& color)
{
	m_Color = color;
}
