#pragma once

#include "shapes/i_shape.h"

class Quad;

class ShapeO : public IShape
{
public:
	ShapeO();
	ShapeO(const Vector3& position);
	
	virtual void Draw();
	
	virtual void Move(Vector3& direction, float dist);
	virtual void RotateLeft();
	virtual void RotateRight();
	
protected:
	virtual void InitializeQuads();
};
