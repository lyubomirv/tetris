#include <precompiled.h>

#include "shapes/quad.h"

#include "settings.h"
#include "lmath/vector3.h"

Quad::Quad()
	: m_Position()
	, m_Color(1.0f, 1.0f, 1.0f)
{
	InitializeVertices();
}

Quad::Quad(const Vector3& position, const Vector3& color)
	: m_Position(position)
{
	Vector3 correctColor = color;
	if(correctColor.x < 0.0f)
		correctColor.x = 0.0f;
	if(correctColor.x > 1.0f)
		correctColor.x = 1.0f;
	if(correctColor.y < 0.0f)
		correctColor.y = 0.0f;
	if(correctColor.y > 1.0f)
		correctColor.y = 1.0f;
	if(correctColor.z < 0.0f)
		correctColor.z = 0.0f;
	if(correctColor.z > 1.0f)
		correctColor.z = 1.0f;
	
	m_Color = color;
		
	InitializeVertices();
}

void Quad::InitializeVertices()
{
	m_Vertices.resize(6);
	
	float dist = Settings::QuadSide / 2;
	
	m_Vertices[0] = Vector3(-dist, -dist, 0.0f);
	m_Vertices[1] = Vector3(-dist, dist, 0.0f);
	m_Vertices[2] = Vector3(dist, dist, 0.0f);
	
	m_Vertices[3] = Vector3(-dist, -dist, 0.0f);
	m_Vertices[4] = Vector3(dist, dist, 0.0f);
	m_Vertices[5] = Vector3(dist, -dist, 0.0f);
}

void Quad::Draw()
{
	glMatrixMode(GL_MODELVIEW);
	
	glTranslatef(m_Position.x, m_Position.y, m_Position.z);
	
	glColor3f(m_Color.x, m_Color.y, m_Color.z);
	
	// Draw the Quad as two triangles
	glBegin(GL_TRIANGLES);
		for(size_t i = 0; i < m_Vertices.size(); ++i)
		{
			glVertex3f(m_Vertices[i].x, m_Vertices[i].y, m_Vertices[i].z);
		}
	glEnd();
	
	// Draw the border of the Quad
	glColor3f(1.0f, 1.0f, 1.0f);
	
	glBegin(GL_LINE_STRIP);
		glVertex3f(m_Vertices[0].x, m_Vertices[0].y, m_Vertices[0].z);
		glVertex3f(m_Vertices[1].x, m_Vertices[1].y, m_Vertices[1].z);
		
		glVertex3f(m_Vertices[1].x, m_Vertices[1].y, m_Vertices[1].z);
		glVertex3f(m_Vertices[2].x, m_Vertices[2].y, m_Vertices[2].z);
		
		glVertex3f(m_Vertices[2].x, m_Vertices[2].y, m_Vertices[2].z);
		glVertex3f(m_Vertices[5].x, m_Vertices[5].y, m_Vertices[5].z);
		
		glVertex3f(m_Vertices[5].x, m_Vertices[5].y, m_Vertices[5].z);
		glVertex3f(m_Vertices[0].x, m_Vertices[0].y, m_Vertices[0].z);
	glEnd();
}

void Quad::Move(Vector3& direction, float dist)
{
	Vector3 temp = direction * dist;
	m_Position += temp;
}
