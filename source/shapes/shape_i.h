#pragma once

#include "shapes/i_shape.h"

class Quad;

class ShapeI : public IShape
{
public:
	ShapeI();
	ShapeI(const Vector3& position);
	
	virtual void Draw();
	
	virtual void Move(Vector3& direction, float dist);
	virtual void RotateLeft();
	virtual void RotateRight();
	
protected:
	virtual void InitializeQuads();
};
