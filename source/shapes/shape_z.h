#pragma once

#include "shapes/i_shape.h"

class Quad;

class ShapeZ : public IShape
{
public:
	ShapeZ();
	ShapeZ(const Vector3& position);
	
	virtual void Draw();
	
	virtual void Move(Vector3& direction, float dist);
	virtual void RotateLeft();
	virtual void RotateRight();
	
private:
	void InitializeQuads();
};
