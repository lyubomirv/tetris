#include "precompiled.h"

#include "shapes/shape_o.h"

#include "shapes/quad.h"

ShapeO::ShapeO()
{
}

ShapeO::ShapeO(const Vector3& position)
{
	m_Position = position;
}

void ShapeO::InitializeQuads()
{
	Vector3 pos;
	Vector3 color = Vector3(1.0f, 1.0f, 0.0f);
	
	Quad* quad = new Quad(pos, color);
	m_Quads.push_back(quad);
	
	pos += Vector3(2.0f, 0.0f, 0.0f);
	quad = new Quad(pos, color);
	m_Quads.push_back(quad);
	
	pos += Vector3(-2.0f, -2.0f, 0.0f);
	quad = new Quad(pos, color);
	m_Quads.push_back(quad);
	
	pos += Vector3(2.0f, 0.0f, 0.0f);
	quad = new Quad(pos, color);
	m_Quads.push_back(quad);
}

void ShapeO::Draw()
{
	glMatrixMode(GL_MODELVIEW);
	
	IShape::QuadList::iterator it = m_Quads.begin();
	for(; it != m_Quads.end(); ++it)
	{
		glLoadIdentity();
		glTranslatef(m_Position.x, m_Position.y, m_Position.z);
		(*it)->Draw();
	}
}

void ShapeO::Move(Vector3& direction, float dist)
{
	Vector3 temp = direction * dist;
	m_Position += temp;
}

void ShapeO::RotateLeft()
{
	// Shape O should not rotate
}

void ShapeO::RotateRight()
{
	// Shape O should not rotate
}

