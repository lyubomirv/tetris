#pragma once

#include <vector>

#include "lmath/vector3.h"

class Quad;

/**
	\brief IShape represents a shape in the game

	All shapes implement this interface

	Implements Factory Method pattern through the InitializeQuads method.
		Each different shape initializes its quads in its own unique way.
*/
class IShape
{
public:
	typedef std::vector<Quad*> QuadList;
	
	virtual ~IShape();

public:
	virtual void Draw() = 0;
	
	virtual void Move(Vector3& direction, float dist) = 0;
	virtual void RotateLeft() = 0;
	virtual void RotateRight() = 0;

public:
	void Initialize();

	int GetQuadsCount() const;
	const QuadList& GetQuads() const;
	
	const Vector3& GetPosition() const;
	void SetPosition(const Vector3& position);

protected:
	virtual void InitializeQuads() = 0;

protected:
	IShape::QuadList m_Quads;
	Vector3 m_Position;
};
