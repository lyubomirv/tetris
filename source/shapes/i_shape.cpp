#include <precompiled.h>

#include "shapes/i_shape.h"

#include <cstdlib>

#include "shapes/quad.h"

IShape::~IShape()
{
	IShape::QuadList::iterator it = m_Quads.begin();
	for(; it != m_Quads.end(); ++it)
	{
		delete (*it);
		*it = NULL;
	}
}

void IShape::Initialize()
{
	InitializeQuads();
}

int IShape::GetQuadsCount() const
{
	return m_Quads.size();
}

const IShape::QuadList& IShape::GetQuads() const
{
	return m_Quads;
}

const Vector3& IShape::GetPosition() const
{
	return m_Position;
}

void IShape::SetPosition(const Vector3& position)
{
	m_Position = position;
}
