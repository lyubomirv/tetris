#include "precompiled.h"

#include "shapes/shape_g.h"

#include "shapes/quad.h"

ShapeG::ShapeG()
{
}

ShapeG::ShapeG(const Vector3& position)
{
	m_Position = position;
}

void ShapeG::InitializeQuads()
{
	Vector3 pos;
	Vector3 color = Vector3(1.0f, 0.0f, 1.0f);
	
	Quad* quad = new Quad(pos, color);
	m_Quads.push_back(quad);
	
	pos += Vector3(0.0f, 2.0f, 0.0f);
	quad = new Quad(pos, color);
	m_Quads.push_back(quad);
	
	pos += Vector3(0.0f, -4.0f, 0.0f);
	quad = new Quad(pos, color);
	m_Quads.push_back(quad);
	
	pos += Vector3(-2.0f, 0.0f, 0.0f);
	quad = new Quad(pos, color);
	m_Quads.push_back(quad);
}

void ShapeG::Draw()
{
	glMatrixMode(GL_MODELVIEW);
	
	IShape::QuadList::iterator it = m_Quads.begin();
	for(; it != m_Quads.end(); ++it)
	{
		glLoadIdentity();
		glTranslatef(m_Position.x, m_Position.y, m_Position.z);
		(*it)->Draw();
	}
}

void ShapeG::Move(Vector3& direction, float dist)
{
	Vector3 temp = direction * dist;
	m_Position += temp;
}

void ShapeG::RotateLeft()
{
	IShape::QuadList::iterator it = m_Quads.begin();
	for(; it != m_Quads.end(); ++it)
	{
		Vector3 quadPos = (*it)->GetPosition();
		quadPos = lmath::Rotate(90.0f, quadPos);
		(*it)->SetPosition(quadPos);
	}
}

void ShapeG::RotateRight()
{
	IShape::QuadList::iterator it = m_Quads.begin();
	for(; it != m_Quads.end(); ++it)
	{
		Vector3 quadPos = (*it)->GetPosition();
		quadPos = lmath::Rotate(-90.0f, quadPos);
		(*it)->SetPosition(quadPos);
	}
}
