//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#include "application.h"

#include <cstring>

#include <SFML/OpenGL.hpp>
#include <SFML/Window.hpp>

#include "input/convert/sfml.h"

Application::Application()
	: _width(400)
	, _height(300)
	, _isActive(true)
	, _nameChanged(false)
{
	memset(_name, 0, NAME_MAX_LENGTH);
}

Application::Application(unsigned int width, unsigned int height, const char* name)
	: _width(width)
	, _height(height)
	, _isActive(true)
{
	if(name && strlen(name) < NAME_MAX_LENGTH)
	{
		strcpy(_name, name);
	}
}

void Application::run()
{
	sf::VideoMode windowedVideoMode(_width, _height, 16);
	sf::VideoMode fullScreenVideoMode = sf::VideoMode::getDesktopMode();

	sf::Window window(windowedVideoMode, _name);

	if(!initialize())
	{
		return;
	}

	sf::Clock clock;
	while(_isActive)
	{
		// Handle events.
		sf::Event event;
		while(window.pollEvent(event))
		{
			switch(event.type)
			{
				case sf::Event::Closed:
					onClosed();
					break;

				case sf::Event::GainedFocus:
					onGainedFocus();
					break;

				case sf::Event::LostFocus:
					onLostFocus();
					break;

				case sf::Event::KeyPressed:
					_input.keyboardKeyPressed(input::sfml::convertKeyboardKey(event.key.code));
					break;

				case sf::Event::KeyReleased:
					_input.keyboardKeyReleased(input::sfml::convertKeyboardKey(event.key.code));
					break;

				case sf::Event::MouseButtonPressed:
					_input.mouseButtonPressed(input::sfml::convertMouseButton(event.mouseButton.button));
					break;

				case sf::Event::MouseButtonReleased:
					_input.mouseButtonReleased(input::sfml::convertMouseButton(event.mouseButton.button));
					break;

				case sf::Event::MouseMoved:
					_input.mouseMoved(event.mouseMove.x, event.mouseMove.y);
					break;

				case sf::Event::MouseWheelMoved:
					_input.mouseWheelMoved(event.mouseWheel.delta);
					break;

				case sf::Event::Resized:
					windowedVideoMode.width = event.size.width;
					windowedVideoMode.height = event.size.height;

					resize(event.size.width, event.size.height);
					break;

				default:
					break;
			}
		}

		// Handle the request for windowed/fullscreen mode change, if present.
		if(_modeChange.requested)
		{
			if(_modeChange.fullscreen)
			{
				window.create(fullScreenVideoMode, _name, sf::Style::Fullscreen);
				window.setMouseCursorVisible(false);
				resize(fullScreenVideoMode.width, fullScreenVideoMode.height);
			}
			else
			{
				window.create(windowedVideoMode, _name, sf::Style::Default);
				window.setMouseCursorVisible(true);
				resize(windowedVideoMode.width, windowedVideoMode.height);
			}

			_modeChange.requested = false;
			_isFullscreen = _modeChange.fullscreen;
		}

		if(_nameChanged)
		{
			window.setTitle(_name);
			_nameChanged = false;
		}

		const sf::Time deltaTime = clock.restart();
		const float deltaTimeSeconds = deltaTime.asSeconds();
		onDraw(deltaTimeSeconds);

		handleInput(deltaTimeSeconds);
		_input.update(deltaTimeSeconds);

		window.display();
	}

	window.close();
}

void Application::stop()
{
	_isActive = false;
}

bool Application::initialize()
{
	return true;
}

void Application::handleInput(float)
{
}

bool Application::isActive() const
{
	return _isActive;
}

unsigned int Application::width() const
{
	return _width;
}

unsigned int Application::height() const
{
	return _height;
}

bool Application::isFullscreen() const
{
	return _isFullscreen;
}

void Application::requestModeChange(bool fullscreen)
{
	_modeChange.requested = true;
	_modeChange.fullscreen = fullscreen;
}

void Application::setName(const char* name)
{
	if(name && strlen(name) < NAME_MAX_LENGTH)
	{
		strcpy(_name, name);
		_nameChanged = true;
	}
}

void Application::onLostFocus()
{
}

void Application::onGainedFocus()
{
}

void Application::onClosed()
{
	_isActive = false;
}

void Application::onResize()
{
}

void Application::onDraw(float)
{
}

void Application::resize(unsigned int width, unsigned int height)
{
	_width = width;
	_height = height;

	glViewport(0, 0, _width, _height);

	_input.setScreenSize(_width, _height);

	onResize();
}
