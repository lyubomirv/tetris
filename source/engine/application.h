//
// Copyright (C) 2012, Lyubomir Vasilev.
//
// This source is subject to the zlib License.
// Please see the LICENSE file for more information.
// All other rights reserved.
//

#pragma once

#include "input/input_system.h"

class Application
{
public:
	Application();
	Application(unsigned int width, unsigned int height, const char* name = NULL);

public:
	/**
	 * \brief Starts the application.
	 *
	 * Creates a window and starts the main loop.
	 */
	void run();

	/**
	 * \brief Stops the application.
	 *
	 * The application will be stopped after the current frame.
	 */
	void stop();

protected:
	/**
	 * \brief Initialization code should go here.
	 *
	 * This will be called when \a run() is called.
	 * \return true if the initialization went ok.
	 */
	virtual bool initialize();

	/**
	 * \brief Handles the application input.
	 */
	virtual void handleInput(float deltaTimeSeconds);

protected:
	/**
	 * \brief Gets the \a isActive flag.
	 *
	 * The application will close if \a isActive is \a false.
	 */
	bool isActive() const;

	unsigned int width() const;
	unsigned int height() const;
	bool isFullscreen() const;

	/**
	 * \brief Request a change in the windowed/fullscreen mode.
	 *
	 * The change is made after the current frame.
	 */
	void requestModeChange(bool fullscreen);

	void setName(const char* name);

protected:
	virtual void onLostFocus();
	virtual void onGainedFocus();

	/**
	 * \brief Called when the user wants to exit the application.
	 *
	 * Override to disable exiting by the 'x' button or for doing additional
	 * logic. Call this when done in case the application should be closed.
	 */
	virtual void onClosed();
	virtual void onResize();
	virtual void onDraw(float deltaTimeSeconds);

private:
	void resize(unsigned int width, unsigned int height);

protected:
	input::InputSystem _input;

private:
	unsigned int _width;
	unsigned int _height;
	bool _isActive;
	bool _isFullscreen;

	enum { NAME_MAX_LENGTH = 256 };
	char _name[NAME_MAX_LENGTH];
	bool _nameChanged;

	struct ModeChange
	{
		ModeChange()
			: requested(false)
			, fullscreen(false)
		{
		}

		bool requested;
		bool fullscreen;
	};
	ModeChange _modeChange;
};
