# This is a configuration file for the cmake build system
# It adds some options and sets some configurations

# Set the strictest warning level
if(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wno-long-long -pedantic")
elseif(MSVC)
	# Force to always compile with W4
	if(CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
		string(REGEX REPLACE "/W[0-4]" "/W4" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
	else()
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")
	endif()
endif()

# Set a default build type if none was specified
if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
	message(STATUS "Setting build type to 'Debug' as none was specified.")
	set(CMAKE_BUILD_TYPE Debug CACHE STRING "Choose the type of build, options are: None(CMAKE_CXX_FLAGS or CMAKE_C_FLAGS used) Debug Release RelWithDebInfo MinSizeRel." FORCE)
	# Set the possible values of build type for cmake-gui
	set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
endif()

# Optionally adding the _DEBUG definition for differentiating between debug and release builds
if(CMAKE_BUILD_TYPE STREQUAL "Debug")
	add_definitions(-D_DEBUG)
endif()

option(ARCHITECTURE_BITS_64 "Compile for 64bit architectures" ON)

# Set a numeric value for the architecture bits
if(ARCHITECTURE_BITS_64)
	message(STATUS "Setting compilation for 64bit architectures.")
	set(ARCHITECTURE_BITS 64)
else()
	message(STATUS "Setting compilation for 32bit architectures.")
	set(ARCHITECTURE_BITS 32)
endif()

# Apply the architecture bits preference
if(ARCHITECTURE_BITS EQUAL 64)
	if(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
		add_definitions(                   -m64)
		set(CMAKE_EXE_LINKER_FLAGS         "${CMAKE_EXE_LINKER_FLAGS} -m64")
		set(CMAKE_SHARED_LIBRARY_C_FLAGS   "${CMAKE_SHARED_LIBRARY_C_FLAGS} -m64")
		set(CMAKE_SHARED_LIBRARY_CXX_FLAGS "${CMAKE_SHARED_LIBRARY_CXX_FLAGS} -m64")
	elseif(MSVC)
		# Probably not possible, cmake has to be started from the correct command prompt
	endif()
elseif(ARCHITECTURE_BITS EQUAL 32)
	if(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
		add_definitions(                   -m32)
		set(CMAKE_EXE_LINKER_FLAGS         "${CMAKE_EXE_LINKER_FLAGS} -m32")
		set(CMAKE_SHARED_LIBRARY_C_FLAGS   "${CMAKE_SHARED_LIBRARY_C_FLAGS} -m32")
		set(CMAKE_SHARED_LIBRARY_CXX_FLAGS "${CMAKE_SHARED_LIBRARY_CXX_FLAGS} -m32")
	elseif(MSVC)
		# Probably not possible, cmake has to be started from the correct command prompt
	endif()
endif()

if(MSVC)
	add_definitions(-D_CRT_SECURE_NO_WARNINGS)
endif()

# Enable testing so that tests can be run.
enable_testing()
